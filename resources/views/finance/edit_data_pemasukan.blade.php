@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ url('/submit_edit_data_masuk') }}" method="post" id="form-data-masuk">
            @csrf

            <input type="hidden" name="id" value="{{ $datamasuk->id }}">

            <div class="judul">
                <label for="tanggal">Tanggal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="date" id="tanggal" name="tanggal" value="{{ $datamasuk->tanggal }}">
            </div>

            <div class="judul">
                <label for="keperluan">Keperluan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="keperluan" name="keperluan" value="{{ $datamasuk->keperluan }}">
            </div>

            <div class="judul">
                <label for="catatan">Catatan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="catatan" name="catatan" value="{{ $datamasuk->catatan }}">
            </div>

            <div class="judul">
                <label for="nominal_in">Nominal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="nominal_in" name="nominal_in" onkeyup="formatRupiah(this)"
                    value="{{ $datamasuk->nominal_in }}">
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>
    <script>
        function formatRupiah(input) {
            // Menghilangkan karakter selain digit dan koma
            let value = input.value.replace(/[^\d,]/g, '');

            // Mengonversi ke angka
            value = parseInt(value || 0, 10);

            // Menambahkan "Rp" dan mengonversi ke format rupiah
            let formattedValue = 'Rp ' + new Intl.NumberFormat('id-ID').format(value);

            // Hanya tambahkan "Rp" pada saat nilai awal kosong atau 0
            if (!value) {
                formattedValue = 'Rp ';
            }

            // Update nilai input
            input.value = formattedValue;
        }

        // Fungsi tambahan untuk membersihkan format rupiah sebelum submit form
        function cleanRupiahFormat(input) {
            // Menghilangkan karakter selain digit dan koma
            let value = input.value.replace(/[^\d,]/g, '');

            // Mengganti koma dengan titik sebagai pemisah desimal
            value = value.replace(/,/g, '.');

            // Mengonversi ke angka desimal
            value = parseFloat(value || 0);

            // Mengonversi ke format angka desimal dengan dua digit di belakang koma
            input.value = value.toFixed(2);
        }

        // Menambahkan event listener untuk membersihkan format rupiah sebelum submit
        document.getElementById('form-data-masuk').addEventListener('submit', function() {
            cleanRupiahFormat(document.getElementById('nominal_in'));
        });
    </script>
@endsection
