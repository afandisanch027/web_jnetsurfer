@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Data Pemasukan
    </div>

    <div class="tampilan-isi">
        <div class="creat">
            <a href="{{ url('/form_data_masuk') }}">Create</a>
        </div>

        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="statusFilter" onchange="filterByStatus()" class="filter1">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>

        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table class="tabel2">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Keperluan</th>
                        <th>Nominal</th>
                        <th>Catatan</th>
                        <th>Action</th>
                    </tr>
                    @php
                        $no = 1;
                    @endphp
                    @foreach ($DataMasuk as $data)
                        @if ($data->nominal_in != 0)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $data->tanggal }}</td>
                                <td>{{ $data->keperluan }}</td>
                                <td>Rp {{ number_format($data->nominal_in, 0, ',', '.') }}</td>
                                <td>{{ $data->catatan }}</td>
                                <td>
                                    <a href="/edit_data_masuk/{{ $data->id }}"><i class='bx bxs-edit'></i></a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
        <div class="page">
            {{ $DataMasuk->links() }}
        </div>
    </div>
@endsection
