@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Laporan Keuangan
    </div>

    <div class="tampilan-isi">
        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="statusFilter" onchange="filterByStatus()" class="filter1">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>

        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table class="tabel2">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Keperluan</th>
                        <th>Catatan</th>
                        <th>Nominal Masuk</th>
                        <th>Nominal Keluar</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    @php
                        $no = 1;
                        $total = 0;
                    @endphp
                    @foreach ($DataLaporan as $data)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $data->tanggal }}</td>
                            <td>{{ $data->keperluan }}</td>
                            <td>{{ $data->catatan }}</td>
                            <td>Rp {{ number_format($data->nominal_in, 0, ',', '.') }}</td>
                            <td>Rp {{ number_format($data->nominal_out, 0, ',', '.') }}</td>
                            <td>Rp {{ number_format($data->nominal_in - $data->nominal_out, 0, ',', '.') }}</td>
                            <td>
                                <a href="/edit_data_masuk/{{ $data->id }}"><i class='bx bxs-edit'></i></a>
                            </td>
                        </tr>
                        @php
                            $total += $data->nominal_in - $data->nominal_out;
                        @endphp
                    @endforeach
                    <tr>
                        <td colspan="6" style="text-align: right; font-weight: bold; border: 1px solid #D9D9D9;">Total Keseluruhan</td>
                        <td style="border: 1px solid #D9D9D9;">Rp {{ number_format($total, 0, ',', '.') }}</td>
                        <td style="border: 1px solid #D9D9D9;"></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="page">
            {{ $DataLaporan->links() }}
        </div>
    </div>
@endsection
