@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.data_masuk') }}" method="post" id="form-data-masuk">
            @csrf

            <div class="judul">
                <label for="tanggal">Tanggal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="date" id="tanggal" name="tanggal">
            </div>

            <div class="judul">
                <label for="keperluan">Keperluan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="keperluan" name="keperluan">
            </div>

            <div class="judul">
                <label for="catatan">Catatan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="catatan" name="catatan">
            </div>

            <div class="judul">
                <label for="nominal_in">Nominal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="nominal_in" name="nominal_in" onkeyup="formatRupiah(this)">
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>

    <script>
        function formatRupiah(input) {
            // Menghilangkan karakter selain digit
            let value = input.value.replace(/[^\d]/g, '');

            // Mengonversi ke angka
            value = parseInt(value || 0, 10);

            // Mengonversi ke format rupiah
            input.value = 'Rp ' + new Intl.NumberFormat('id-ID').format(value);
        }

        // Fungsi tambahan untuk membersihkan format rupiah sebelum submit form
        function cleanRupiahFormat(input) {
            // Menghilangkan karakter selain digit dan koma
            let value = input.value.replace(/[^\d,]/g, '');

            // Mengganti koma dengan titik sebagai pemisah desimal
            value = value.replace(/,/g, '.');

            // Mengonversi ke angka desimal
            value = parseFloat(value || 0);

            // Mengonversi ke format angka desimal dengan dua digit di belakang koma
            input.value = value.toFixed(2);
        }

        // Menambahkan event listener untuk membersihkan format rupiah sebelum submit
        document.getElementById('form-data-masuk').addEventListener('submit', function() {
            cleanRupiahFormat(document.getElementById('nominal_in'));
        });
    </script>
@endsection
