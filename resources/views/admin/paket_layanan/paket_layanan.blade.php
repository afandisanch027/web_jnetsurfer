@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Paket Layanan
    </div>

    <div class="tampilan-isi">
        <div class="creat">
            <a href="{{ url('/form_paket_layanan') }}">Create</a>
        </div>

        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="statusFilter" onchange="filterByStatus()" class="filter1">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>

        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table class="tabel2">
                    <tr>
                        <th class="corner-awal">No</th>
                        <th>Kecepatan</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Type</th>
                        <th class="corner-end">Action</th>
                    </tr>
                    @foreach($paketlayanan as $index => $paket)
                        <tr>
                            <td class="corner-awal">{{ $index + 1 }}</td>
                            <td>{{ $paket->kecepatan }}</td>
                            <td>{{ $paket->satuan }}</td>
                            <td>Rp {{ number_format($paket->harga, 0, ',', '.') }}</td>
                            <td>{{ $paket->type }}</td>
                            <td class="corner-end">
                                <!-- Tambahkan tombol-tombol aksi jika diperlukan -->
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
        <div class="page">
            {{ $paketlayanan->links() }}
        </div>
    </div>

@endsection
