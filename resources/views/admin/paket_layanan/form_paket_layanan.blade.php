@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.paket_layanan') }}" method="post" id="form-paket-layanan">
            @csrf

            <div class="judul">
                <label for="kecepatan">Kecepatan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="number" id="kecepatan" name="kecepatan" min="0">
            </div>

            <div class="judul">
                <label for="satuan">Satuan</label>
            </div>
            <div class="keterangan">
                <select class="input1" id="satuan" name="satuan">
                    <option value="Mbps">Mbps</option>
                </select>
            </div>

            <div class="judul">
                <label for="harga">Harga</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="harga" name="harga" onkeyup="formatRupiah(this)">
            </div>

            <div class="judul">
                <label for="type">Type</label>
            </div>
            <div class="keterangan">
                <select class="input1" id="type" name="type">
                    <option value="bln">bln</option>
                </select>
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>

    <script>
        function formatRupiah(input) {
            // Menghilangkan karakter selain digit
            let value = input.value.replace(/[^\d]/g, '');

            // Mengonversi ke angka
            value = parseInt(value || 0, 10);

            // Mengonversi ke format rupiah
            input.value = 'Rp ' + new Intl.NumberFormat('id-ID').format(value);
        }

        // Fungsi tambahan untuk membersihkan format rupiah sebelum submit form
        function cleanRupiahFormat(input) {
            // Menghilangkan karakter selain digit dan koma
            let value = input.value.replace(/[^\d,]/g, '');

            // Mengganti koma dengan titik sebagai pemisah desimal
            value = value.replace(/,/g, '.');

            // Mengonversi ke angka desimal
            value = parseFloat(value || 0);

            // Mengonversi ke format angka desimal dengan dua digit di belakang koma
            input.value = value.toFixed(2);
        }

        // Menambahkan event listener untuk membersihkan format rupiah sebelum submit
        document.getElementById('form-paket-layanan').addEventListener('submit', function () {
            cleanRupiahFormat(document.getElementById('harga'));
        });
    </script>
@endsection
