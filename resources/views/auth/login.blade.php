<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Awinet | Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }
        body{
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background: #69cf9d;
        }
        .container-login{
            width: 100%;
            height: 85vh;
            display: flex;
            max-width: 85%;
            background: #ffffff;
            border-radius: 20px;
            box-shadow: 0 10px 15px rgba(0, 0, 0, 0.1);
        }
        .login{
            margin-block-start: 8%;
            width: 50%;

        }
        form{
            width: 80%;
            margin: 60px auto;
        }
        h1{
            margin: 20px;
            text-align: center;
            font-weight: bolder;
            text-transform: uppercase;
        }
         /* hr{
            border-top: 2px solid #7676d7; 
        }*/
        p{
            text-align: end;
            margin: 10px; 
            margin-inline-end: 8%;  
        }
        a{
            color: #B9B7B7;
        }
        .right img{
            
            /* width: 100px; */
            height: 22%;
            margin-block-start: 50%;
            margin-inline-start: 25%;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
            
        }
        form label{
            display: block;
            font-size: 16px;
            font-weight: 600;
            padding: 5px;
        }
        input{
            width: 85%;
            height: 5vh;
            margin: 10px;
            border: none;
            outline: none;
            padding: 8px;
            border-radius: 20px;
            background-color: #D9D9D9;
            margin-inline-start: 10%;
        }
        button{
            margin-inline-start: 10%;
            border: none;
            outline: none;
            padding: 8px;
            width: 85%;
            height: 6vh;
            color: #ffffff;
            font-size: 16px;
            cursor: pointer;
            margin-top: 20px;
            border-radius: 18px;
            background: #FF8A48;
        }
        button:hover{
            background: rgb(105, 207, 157)
        }
        @media (max-width : 880px){
            .container-login{
                width: 100%;
                max-width: 750px;
                margin-left: 20px;
                margin-right: 20px;
            }
            form{
                width: 300px;
                margin: 200px auto;
            }
            .login{
                width: 400px;
                padding: 20px;
            }
            button{
                width: 100%;
            }
            /* .right img{
                width: 100%;
                height: 100%;
            } */
        }
    </style>
</head>
<body>
    <div class="container-login">
        <div class="login">
            <form action="{{ route('login-proses') }}" method="post">
                @csrf
                <h1>LOG-IN</h1>
                <input type="email" name="email" placeholder="example@gmail.com">
                @error('email')
                    <small>{{ $message }}</small>
                @enderror
                <input type="password" name="password" placeholder="Password">
                @error('password')
                    <small>{{ $message }}</small>
                @enderror
                <p>
                    <a href="{{ route('register') }}">Register</a>
                </p>
                <button name="submit" type="submit">Log-In</button>

            </form>
        </div>
        <div class="right">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU" alt="">
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    @if ($message = Session::get('success'))
        <script>Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: '{{ $message }}',
          })</script>
    @endif
    @if ($message = Session::get('failed'))
    <script>Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: '{{ $message }}',
      })</script>
@endif
</body>
</html>

