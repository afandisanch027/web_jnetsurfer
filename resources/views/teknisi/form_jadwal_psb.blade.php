@extends('layouts.app')

@section('title', 'Jadwal Survei')

@section('content')
    <div class="tampilan-dashboard">
        Jadwal PSB
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.jadwal_psb') }}" method="POST">
            @csrf
            <input type="hidden" name="sales_id" value="{{ $salesData->id }}">
            <input type="hidden" name="status" value="waiting_psb">
            <div class="kiri">
                <div class="judul1">Nama Pelanggan</div>
                <div class="keterangan1">{{ $salesData->plgName }}</div>
                <div class="judul1">Nomer Ticket</div>
                <div class="keterangan1">{{ $salesData->noTicket }}</div>
                <div class="judul1">Nomer Telephone</div>
                <div class="keterangan1">{{ $salesData->noHp }}</div>


            </div>

            <div class="kanan">
                <div class="judul1">Alamat</div>
                <div class="keterangan1">Jalan {{ $salesData->jalan }}, RT {{ $salesData->rt }}, RW
                    {{ $salesData->rw }},<br> Kelurahan/Desa {{ $salesData->kelurahan_desa }}, Kecamatan
                    {{ $salesData->kecamatan }}, Kabupaten {{ $salesData->kabupaten }}</div>
                <div class="judul1">Tanggal Survei*</div>
                <div class="keterangan1">
                    <input type="date" name="tgl_psb" id="date_timepicker_end">
                </div>
                <div class="judul1">Team Teknisi*</div>
                <div class="keterangan1">
                    <select id="negara" name="team">
                        <option value="team_1">Team 1</option>
                        <option value="team_2">Team 2</option>
                        <option value="team_3">Team 3</option>
                    </select>
                </div>

                <button>Terima</button>
            </div>
        </form>
    </div>
@endsection
