@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Teknisi
    </div>
    <div class="tampilan-isi">
        <select id="statusFilter" onchange="filterByStatus()" class="filter">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>
        <div class="search">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <table>
            <thead>
                <tr>
                    <th class="corner-awal">No Ticket</th>
                    <th>Tanggal Survei</th>
                    <th>Nama Pelanggan</th>
                    <th>No. Telp</th>
                    <th>Paket</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th class="corner-end">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($salesData as $sales)
                    @php
                        // Dapatkan status dari Teknisi berdasarkan sales_id
                        $teknisi = $teknisiData->where('sales_id', $sales->id)->first();
                        $status = $teknisi ? $teknisi->status : 'Belum Ada Survei';
                        $tgl_survei = $teknisi ? $teknisi->tgl_survei : '-';
                    @endphp
                    @if($teknisi)
                    <tr>
                        <td class="corner-awal">{{ $sales->noTicket }}</td>
                        <td>{{ $tgl_survei }}</td>
                        <td>{{ $sales->plgName }}</td>
                        <td>{{ $sales->noHp }}</td>
                        <td>{{ $sales->paket }}</td>
                        <td>{{ $sales->harga }}</td>
                        <td>{{ $status }}</td>
                        <td class="corner-end">

                        </td>
                    </tr>
                    @endif
                    @endforeach
            </tbody>

        </table>
        <div class="page">
            {{ $salesData->links() }}
        </div>

    </div>
@endsection
