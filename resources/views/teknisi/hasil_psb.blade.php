@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Teknisi PSB
    </div>
    <div class="tampilan-isi">
        <select id="statusFilter" onchange="filterByStatus()" class="filter">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>
        <div class="search">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <table>
            <thead>
                <tr>
                    <th class="corner-awal">No Ticket</th>
                    <th>Tanggal Psb</th>
                    <th>Nama Pelanggan</th>
                    <th>No. Telp</th>
                    <th>Paket</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th class="corner-end">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($salesData as $sales)
                    @php
                        $jadwal_psb = $jadwal_psbData->where('sales_id', $sales->id)->first();
                        $status_psb = $jadwal_psb ? $jadwal_psb->status : 'Belum Ada Survei';
                        $tgl_psb = $jadwal_psb ? $jadwal_psb->tgl_psb : '-';
                    @endphp
                    @if ($jadwal_psb)
                        <tr>
                            <td class="corner-awal">{{ $sales->noTicket }}</td>
                            <td>{{ $tgl_psb ?? '-' }}</td>
                            <td>{{ $sales->plgName }}</td>
                            <td>{{ $sales->noHp }}</td>
                            <td>{{ $sales->paket }}</td>
                            <td>{{ $sales->harga }}</td>
                            <td>{{ $status_psb }}</td>
                            <td class="corner-end">
                                @if ($status_psb == 'Waiting_Psb')
                                    <a href="{{ url('form_jadwal_psb/' . $sales->id) }}"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Proses_Psb')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Selesai_Psb')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Waiting_Teknisi_Psb')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Proses_Teknisi_Psb')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Selesai_Teknisi_Psb')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Aktivasi_Psb')
                                    <a href="{{ url('form_aktivasi_psb/' . $sales->id) }}"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_psb == 'Selesai')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @else
                                    {{ $status_psb }}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>

        </table>
        <div class="page">
            {{ $salesData->links() }}
        </div>

    </div>
@endsection
