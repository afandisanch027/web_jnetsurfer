@extends('layouts.app')

@section('title', 'Jadwal Survei')

@section('content')
    <div class="tampilan-dashboard">
        Aktivasi PSB
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.aktivasi_psb') }}" method="POST">
            @csrf
            <input type="hidden" name="sales_id" value="{{ $salesData->id }}">
            <div class="kiri">
                <div class="judul1">Nama Pelanggan</div>
                <div class="keterangan1">{{ $salesData->plgName }}</div>
                <div class="judul1">Nomer Ticket</div>
                <div class="keterangan1">{{ $salesData->noTicket }}</div>
                <div class="judul1">Metode Pembayaran</div>
                <div class="keterangan1">
                    @foreach ($paying as $payment)
                        <div>{{ $payment->metode }}</div>
                    @endforeach
                </div>
            </div>

            <div class="tengah">
                <div class="judul1">Paket Layanan</div>
                <div class="keterangan1">{{ $salesData->paket }} /{{ $salesData->harga }}</div>
                <div class="judul1">Nomer Telephone</div>
                <div class="keterangan1">{{ $salesData->noHp }}</div>
                <div class="judul1">Bukti Pembayaran</div>
                <div class="keterangan1">
                    @foreach ($paying as $payment)
                        <img src="{{ $payment->bukti_pembayaran }}" alt="Bukti Pembayaran">
                    @endforeach
                </div>
            </div>

            <div class="kanan">
                <div class="judul1">Alamat</div>
                <div class="keterangan1">Jalan {{ $salesData->jalan }}, RT {{ $salesData->rt }}, RW
                    {{ $salesData->rw }}, Kelurahan/Desa {{ $salesData->kelurahan_desa }}, <br>
                    Kecamatan
                    {{ $salesData->kecamatan }}, Kabupaten {{ $salesData->kabupaten }}</div>

                <button>Terima</button>
            </div>
        </form>
    </div>
@endsection
