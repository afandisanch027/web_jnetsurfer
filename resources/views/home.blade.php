@extends('layouts.app')

@section('title', 'Home Page')

@section('content')

    {{-- tampilan isi dashboard admin --}}
    @can('view_dashboard_admin')
        <div class="tampilan-dashboard">
            Dashboard
        </div>
        <div class="tampilan-dashboard1">
            <div class="tampilan-dash1">
                <div class="dash-icons">
                    <i class='bx bx-file fa-4x'></i>
                </div>
                <div class="dash-keterangan">
                    Sales
                </div>
                <div class="dash-notif">
                    3
                </div>
            </div>

            <div class="tampilan-dash2">
                <div class="dash-icons">
                    <i class='bx bxs-box  fa-4x'></i>
                </div>
                <div class="dash-keterangan">
                    Inventory
                </div>
                <div class="dash-notif">
                    5
                </div>
            </div>
        </div>

        <div class="tampilan-dashboard2">

            <div class="tampilan-dash3">
                <div class="dash-icons">
                    <i class='bx bx-wrench  fa-4x'></i>
                </div>
                <div class="dash-keterangan">
                    Teknisi
                </div>
                <div class="dash-notif">
                    10
                </div>
            </div>

            <div class="tampilan-dash4">
                <div class="dash-icons">
                    <i class='bx bx-dollar-circle  fa-4x'></i>
                </div>
                <div class="dash-keterangan">
                    Finance
                </div>
                <div class="dash-notif">
                    10
                </div>
            </div>
        </div>

        <div class="diagram"><img src="https://www.jojonomic.com/wp-content/uploads/2020/12/11-1.jpg" alt=""></div>

        </div>
    @endcan

    {{-- tampilan isi dashboard inventory --}}
    @can('view_dashboard_inventory')
        <div class="tampilan-dashboard">
            Dashboard
        </div>

        <div class="tamp1">
            <div class="tamp1_1">
                <i class='bx bx-package fa-3x''></i>
                <p>Permintaan Baru</p>
                <br>
                <p class="notif">10</p>
            </div>
            <div class="tamp1_2">
                <i class='bx bx-archive-in fa-3x'></i>
                <p>Data Barang Masuk</p>
                <br>
                <p class="notif">13</p>
            </div>
            <div class="tamp1_3">
                <i class='bx bx-archive-out fa-3x'></i>
                <p>Data Barang Keluar</p>
                <br>
                <p class="notif">08</p>
            </div>
        </div>

        <div class="tamp2">
            <div class="tamp2_1">
                <i class='bx bx-file fa-3x'></i>
                <p>Kategori</p>
                <br>
                <p class="notif1">34</p>
            </div>
            <div class="tamp2_2">
                <i class='bx bx-folder-open fa-3x'></i>
                <p>Data Satuan</p>
                <br>
                <p class="notif1">4</p>

            </div>
        </div>

        <div class="tamp3">
            <div class="jud">Stock mencapai batas maksimum</div>
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            {{-- <div class="tamp3_1">
                <table class="tabel1">
                    <tr>
                        <th>No</th>
                        <th>ID Barang</th>
                        <th>Nama Barang</th>
                        <th>Kategori</th>
                        <th>Quantity</th>
                        <th>Satuan</th>

                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>


                    </tr>
                </table>
            </div> --}}
        </div>
    @endcan

    {{-- tampilan isi dashboard finance --}}
    @can('view_dashboard_finance')
        <div class="tampilan-dashboard">
            Dashboard
        </div>

        <div class="tampil1">
            <p class="tampil1_1">Total Income</p>
            <br>
            <p class="tampil1_2">Rp. 2.500.00,-</p>
        </div>

        <div class="tampil2">
            <p class="tampil2_3">Total Expenses</p>
            <br>
            <p class="tampil2_4">Rp. 10.500.00,-</p>
        </div>


        <img src="https://wikielektronika.com/wp-content/uploads/2022/01/diagram-garis-tunggal.jpg" alt="">

        @endcan

    @endsection
