<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">
</head>

<body>
    <header>
        @can('view_dashboard_admin')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/admin">Admin</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a href="
                    {{ route('logout') }}
                    "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan

        {{-- menu inventory --}}
        @can('view_dashboard_inventory')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/admin">Gudang</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a href="
                            {{ route('logout') }}
                            "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan

        {{-- menu Finance --}}
        @can('view_dashboard_finance')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/admin">Finance</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a
                                href="
                                            {{ route('logout') }}
                                            "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan
    </header>

    <aside>
        {{-- sidebar_admin --}}
        @can('view_dashboard_admin')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/admin"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-file'></i>SALES<i class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('sales.index') }}" class="sub-item"><i class='bx bx-notepad'></i>Survei Baru</a>
                            <a href="#" class="sub-item"><i class='bx bx-notepad'></i>Data Pelanggan</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>TEKNISI<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('teknisi.index') }}" class="sub-item"><i class='bx bx-notepad'></i>Survei</a>
                            <a href="{{ route('teknisi.psb') }}" class="sub-item"><i class='bx bx-notepad'></i>Pemasangan Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('inventory.stock_barang') }}" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('finance.data_laporan') }}" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan
                                Keuangan</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-cog'></i>SETTINGS<i class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="#" class="sub-item"><i class='bx bxs-user-account'></i></i>Account</a>
                            <a href="{{ route('paketlayanan.index') }}" class="sub-item"><i class='bx bxs-user-account'></i></i>Paket Layanan</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        {{-- sidebar_finance --}}
        @can('view_dashboard_finance')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/admin"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>FINANCE<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('finance.data_pemasukan') }}" class="sub-item"><i class='bx bx-notepad'></i>Data
                                Pemasukan</a>
                            <a href="{{ route('finance.data_pengeluaran') }}" class="sub-item"><i class='bx bx-notepad'></i>Data
                                Pengeluaran</a>
                            <a href="{{ route('finance.data_laporan') }}" class="sub-item"><i class='bx bx-notepad'></i>Laporan
                                Keuangan</a>
                            <a href="{{ route('finance.data_komisi') }}" class="sub-item"><i class='bx bx-notepad'></i>Komisi</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        {{-- sidebar_Gudang --}}
        @can('view_dashboard_inventory')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/admin"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-file'></i>GUDANG<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('inventory.index') }}" class="sub-item"><i class='bx bx-notepad'></i>Permintaan
                                Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>BARANG<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('inventory.data_barang') }}" class="sub-item"><i class='bx bx-notepad'></i>Data Barang</a>
                            <a href="{{ route('inventory.stock_barang') }}" class="sub-item"><i class='bx bx-notepad'></i>Stock Barang</a>
                            <a href="{{ route('inventory.barang_masuk') }}" class="sub-item"><i class='bx bx-notepad'></i>Data Barang Masuk</a>
                            <a href="{{ route('inventory.barang_keluar') }}" class="sub-item"><i class='bx bx-notepad'></i>Data Barang
                                Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        <main>
            @yield('content')
        </main>


    </aside>
{{-- 
    <main>
        @yield('content')
    </main> --}}
    

    <script>
        var currentFilter = '';
        function search() {
            var input, filter, table, tr, td, i, j, txtValue;
            input = document.getElementById('searchInput');
            filter = input.value.toUpperCase();
            table = document.querySelector('.tampilan-isi table');
            tr = table.getElementsByTagName('tr');
            currentFilter = filter; // Simpan status pencarian saat ini
            for (i = 0; i < tr.length; i++) {
                var found = false;
                td = tr[i].getElementsByTagName('td');
                for (j = 0; j < td.length; j++) {
                    if (td[j]) {
                        txtValue = td[j].textContent || td[j].innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1) {
                            found = true;
                            break;
                        }
                    }
                }
                if (found) {
                    tr[i].style.display = '';
                } else {
                    if (i > 0) {
                        tr[i].style.display = 'none'; // Hide rows, but keep the header row visible
                    }
                }
            }
        }
        function filterByStatus() {
            var selectedStatus = document.getElementById('statusFilter').value;
            var table, tr, td, i;
            table = document.querySelector('.tampilan-isi table');
            tr = table.getElementsByTagName('tr');
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName('td')[5];
                if (td) {
                    if (selectedStatus === 'all' || td.innerText === selectedStatus) {
                        tr[i].style.display = '';
                    } else {
                        tr[i].style.display = 'none';
                    }
                }
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.sub-btn').click(function() {
                $(this).next('.sub-menu').slideToggle();
                $(this).find('.dropdown').toggleClass('rotate');
            });
        });
    </script>
</body>
</html>
