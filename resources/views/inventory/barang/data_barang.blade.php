@extends('layouts.app')

@section('title', 'Awinet | Data Barang')

@section('content')
    <div class="tampilan-dashboard">
        Data Barang
    </div>

    <div class="tampilan-isi">
        <div class="creat">
            <a href="{{ url('/form_data_barang') }}">Create</a>
        </div>

        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="typeFilter" onchange="filterByType()" class="filter1">
            <option value="all">Filter</option>
            <option value="Stockable">Stockable</option>
            <option value="Consumable">Consumable</option>
        </select>

        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table class="tabel2">
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Kategori</th>
                        <th>Satuan</th>
                        <th>Type</th>
                        <th>Safety Stock</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($masterBarangs as $data)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $data->nama_barang }}</td>
                            <td>{{ $data->kategori }}</td>
                            <td>{{ $data->satuan }}</td>
                            <td>{{ $data->type }}</td>
                            <td>{{ $data->safety_stock }}</td>
                            <td>
                                <a href="/edit_data_barang/{{ $data->id }}"><i class='bx bxs-edit'></i></a>
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
        <div class="page">
            {{ $masterBarangs->links() }}
        </div>

        <script>
            var currentFilter = '';

            function search() {
                var input, filter, table, tr, td, i, j, txtValue;
                input = document.getElementById('searchInput');
                filter = input.value.toUpperCase();
                table = document.querySelector('.tampilan-isi table');
                tr = table.getElementsByTagName('tr');
                currentFilter = filter; // Simpan status pencarian saat ini
                for (i = 0; i < tr.length; i++) {
                    var found = false;
                    td = tr[i].getElementsByTagName('td');
                    for (j = 0; j < td.length; j++) {
                        if (td[j]) {
                            txtValue = td[j].textContent || td[j].innerText;
                            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (found) {
                        tr[i].style.display = '';
                    } else {
                        if (i > 0) {
                            tr[i].style.display = 'none'; // Hide rows, but keep the header row visible
                        }
                    }
                }
            }

            function filterByType() {
                var selectedType = document.getElementById('typeFilter')
                .value; // Perbarui id dari 'statusFilter' menjadi 'typeFilter'
                var table, tr, td, i;
                table = document.querySelector('.tampilan-isi table');
                tr = table.getElementsByTagName('tr');
                for (i = 0; i < tr.length; i++) {
                    td = tr[i].getElementsByTagName('td')[4]; // Perbarui indeks dari 5 menjadi 4 (indeks kolom tipe)
                    if (td) {
                        if (selectedType === 'all' || td.innerText === selectedType) {
                            tr[i].style.display = '';
                        } else {
                            tr[i].style.display = 'none';
                        }
                    }
                }
            }
        </script>
    </div>

@endsection
