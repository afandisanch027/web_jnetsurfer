@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ url('/submit_edit_data_barang') }}" method="post">
            @csrf

            <input type="hidden" name="id" value="{{ $databarang->id }}">

            <div class="judul">
                <label for="nama_barang">Nama Barang</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="nama_barang" name="nama_barang" value="{{ $databarang->nama_barang }}">
            </div>

            <div class="judul">
                <label for="kategori">Kategori</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="kategori" name="kategori" value="{{ $databarang->kategori }}">
            </div>

            <div class="judul">
                <label for="satuan">Satuan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="satuan" name="satuan" value="{{ $databarang->satuan }}">
            </div>

            <div class="judul">
                <label for="type">Type</label>
            </div>
            <div class="keterangan">
                <select id="type" name="type" class="input1">
                    <option value="Stockable" {{ $databarang->type == 'Stockable' ? 'selected' : '' }}>Stockable</option>
                    <option value="Consumable" {{ $databarang->type == 'Consumable' ? 'selected' : '' }}>Consumable</option>
                </select>
            </div>

            <div class="judul">
                <label for="safety_stock">Safety Stock</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="safety_stock" name="safety_stock" value="{{ $databarang->safety_stock }}">
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>
@endsection
