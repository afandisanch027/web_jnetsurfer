@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.barang_masuk') }}" method="post">
            @csrf

            <div class="judul">
                <label for="tanggal">Tanggal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="date" id="tanggal" name="tanggal">
            </div>

            <div class="judul">
                <label for="nama_barang">Nama Barang</label>
            </div>
            <div class="keterangan">
                <select class="input1" id="nama_barang" name="nama_barang">
                    @foreach($masterBarangs as $barang)
                        <option value="{{ $barang->id }}">{{ $barang->nama_barang }}</option>
                    @endforeach
                </select>
            </div>

            <div class="judul">
                <label for="quantity">Quantity</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="quantity" name="quantity">
            </div>


            <button type="submit">Terima</button>
        </form>
    </div>
@endsection
