@extends('layouts.app')

@section('title', 'Stock Inventory')

@section('content')
    <div class="tampilan-dashboard">
        Stock Barang
    </div>

    <div class="tampilan-isi">
        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="statusFilter" onchange="filterByStatus()" class="filter1">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>
        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Kategori</th>
                        <th>Satuan</th>
                        <th>Type</th>
                        <th>Quantity Masuk</th>
                        <th>Quantity Keluar</th>
                        <th>Sisa Stok</th>
                    </tr>
                    @foreach ($masterBarangs as $index => $barang)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $barang->nama_barang ?? '-' }}</td>
                            <td>{{ $barang->kategori ?? '-' }}</td>
                            <td>{{ $barang->satuan ?? '-' }}</td>
                            <td>{{ $barang->type ?? '-' }}</td>
                            <td>{{ $barangMasuk[$barang->id] ?? 0 }}</td>
                            <td>{{ $barangKeluar[$barang->id] ?? 0 }}</td>
                            <td>{{ $barangMasuk[$barang->id] - $barangKeluar[$barang->id] ?? 0 }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
