@extends('layouts.app')

@section('title', 'Awinet | Form Data Barang')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.data_barang') }}" method="post">
            @csrf

            <div class="judul">
                <label for="nama_barang">Nama Barang</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="nama_barang" name="nama_barang">
            </div>

            <div class="judul">
                <label for="kategori">Kategori</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="kategori" name="kategori">
            </div>
            <div class="judul">
                <label for="satuan">Satuan</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="satuan" name="satuan">
            </div>
            <div class="judul">
                <label for="type">Type</label>
            </div>
            <div class="keterangan">
                <select id="type" name="type" class="input1">
                    <option value="Stockable">Stockable</option>
                    <option value="Consumable">Consumable</option>
                </select>
            </div>

            <div class="judul">
                <label for="safety_stock">Safety Stock</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="safety_stock" name="safety_stock">
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>
@endsection
