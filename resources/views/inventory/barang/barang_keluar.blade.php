@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Barang Keluar
    </div>

    <div class="tampilan-isi">
        <div class="creat">
            <a href="{{ url('/form_barang_keluar') }}">Create</a>
        </div>

        <div class="search1">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <select id="statusFilter" onchange="filterByStatus()" class="filter1">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>

        <div class="tamp4">
            <div class="garis_horizontal"></div>
            <div class="ket1">
                Tampilkan 10 data
            </div>
            <div class="tamp4_1">
                <table class="tabel2">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama Barang</th>
                        <th>Kategori</th>
                        <th>Quantity</th>
                        <th>Satuan</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($barangkeluar as $data)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $data->tanggal }}</td>
                            <td>{{ $data->masterBarang->nama_barang }}</td>
                            <td>{{ $data->masterBarang->kategori }}</td>
                            <td>{{ $data->quantity }}</td>
                            <td>{{ $data->masterBarang->satuan }}</td>
                            <td>
                                <a href="/edit_barang_keluar/{{ $data->id }}"><i class='bx bxs-edit'></i></a>
                            </td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>
        <div class="page">
            {{ $barangkeluar->links() }}
        </div>
    </div>

@endsection
