@extends('layouts.app')

@section('title', 'Survei')

@section('content')
    <div class="tampilan-dashboard">
        Create
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit_edit_barang_masuk', ['id' => $barangmasuk->id]) }}" method="post">
            @csrf

            <input type="hidden" name="id" value="{{ $barangmasuk->id }}">

            <div class="judul">
                <label for="tanggal">Tanggal</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="date" id="tanggal" name="tanggal" value="{{ $barangmasuk->tanggal }}">
            </div>

            <div class="judul">
                <label for="id_barang">Nama Barang</label>
            </div>
            <div class="keterangan">
                <select class="input1" id="id_barang" name="id_barang">
                    @foreach($masterBarangs as $barang)
                        <option value="{{ $barang->id }}" @if($barang->id === $barangmasuk->id_barang) selected @endif>{{ $barang->nama_barang }}</option>
                    @endforeach
                </select>
            </div>

            <div class="judul">
                <label for="quantity">Quantity</label>
            </div>
            <div class="keterangan">
                <input class="input1" type="text" id="quantity" name="quantity" value="{{ $barangmasuk->quantity }}">
            </div>

            <button type="submit">Terima</button>
        </form>
    </div>
@endsection
