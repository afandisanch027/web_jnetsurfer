@extends('layouts.app')

@section('title', 'Jadwal Survei')

@section('content')
    <div class="tampilan-dashboard">
        Pengecekan Barang
    </div>

    <div class="tampilan-isi">
        <form action="{{ route('submit.gudang_cek') }}" method="POST">
            @csrf
            <input type="hidden" name="sales_id" value="{{ $salesData->id }}">
            <input type="hidden" name="status" value="waiting_survei">

            <div class="kiri">
                <div class="judul1">Nama Pelanggan</div>
                <div class="keterangan1">{{ $salesData->plgName }}</div>
                <div class="judul1">Nomer Ticket</div>
                <div class="keterangan1">{{ $salesData->noTicket }}</div>
                <div class="judul1">Tanggal Pemasangan Baru</div>
                <div class="keterangan1">{{ optional($jadwalPsb)->tgl_psb ?? '-' }}</div>
                <div class="judul1">Nomer Telephone</div>
                <div class="keterangan1">{{ $salesData->noHp }}</div>
            </div>

            <div class="kanan">
                <div class="judul1">Kebutuhan Barang</div>
                <div class="keterangan1">
                    @foreach ($barangs as $barang)
                        @if ($barang->masterBarang)
                            <div>
                                <strong>{{ $barang->masterBarang->nama_barang }}</strong> <br>
                                Quantity: {{ $barang->quantity }}
                                {{ $barang->masterBarang->satuan }}
                                <br>
                            </div>
                        @endif
                    @endforeach
                </div>

                <button>Terima</button>
            </div>
        </form>
    </div>
@endsection
