@extends('layouts.app')

@section('title', 'Inventory')

@section('content')
    <div class="tampilan-dashboard">
        Teknisi PSB
    </div>
    <div class="tampilan-isi">
        <select id="statusFilter" onchange="filterByStatus()" class="filter">
            <option value="all">Filter</option>
            <option value="Waiting">Waiting</option>
            <option value="Proses">Proses</option>
            <option value="Selesai">Selesai</option>
        </select>
        <div class="search">
            <input type="text" id="searchInput" placeholder="Cari">
            <button onclick="search()"><i class='bx bx-search'></i></button>
        </div>
        <table>
            <thead>
                <tr>
                    <th class="corner-awal">No Ticket</th>
                    <th>Tanggal Psb</th>
                    <th>Nama Pelanggan</th>
                    <th>No. Telp</th>
                    <th>Paket</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th class="corner-end">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($salesData as $sales)
                    @php
                        $gudang_cek = $gudang_cekData->where('sales_id', $sales->id)->first();
                        $tgl_psb = optional($sales->jadwalPsb)->tgl_psb;
                        $status_cek = $gudang_cek ? $gudang_cek->status : 'Belum Ada Survei';
                    @endphp

                    @if ($status_cek != 'Belum Ada Survei')
                        <tr>
                            <td class="corner-awal">{{ $sales->noTicket }}</td>
                            <td>{{ $tgl_psb ?? '-' }}</td>
                            <td>{{ $sales->plgName }}</td>
                            <td>{{ $sales->noHp }}</td>
                            <td>{{ $sales->paket }}</td>
                            <td>{{ $sales->harga }}</td>
                            <td>{{ $status_cek }}</td>
                            <td class="corner-end">
                                @if ($status_cek == 'Waiting_Cek')
                                    <a href="{{ url('form_gudang_cek/' . $sales->id) }}"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_cek == 'Proses_Cek')
                                    <a href="{{ url('#' . $sales->id) }}"><i class='bx bxs-edit'></i></a>
                                @elseif ($status_cek == 'Selesai_Cek')
                                    <a href="{{ url('#' . $sales->id) }}"><i class='bx bxs-edit'></i></a>
                                @else
                                    {{ $status_cek }}
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
        <div class="page">
            {{ $salesData->links() }}
        </div>
    </div>
@endsection
