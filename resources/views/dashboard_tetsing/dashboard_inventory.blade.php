<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Awinet | Admin</title>
  <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
  <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">


  <style type="text/css">
    *{
      font-family: 'Poppins', sans-serif;
      padding: 0;
      margin: 0;
    }
    .wrap{
    background: #E8E8E8;
    width: 100%;
    height: 100vh;
    margin: 0px;
    padding: 0px;

}
    

/* bagian header
.wrap .header{
    background: green;
    /*height: 50px;*/
    /* padding: 2px 10px;
} */

/*akhir header*/


/*bagian menu*/
.wrap .menu{
    background: #69CF9D;
    
}

.wrap .menu ul{
    padding: 0;
    margin: 0;
    
    overflow: hidden;
}

.wrap .menu ul li{
    float: left;
    list-style-type: none;
    padding: 10px;
}
.wrap .menu ul li a{
    font-size: 16px;
    color: white;
    text-decoration: none;
    font-weight: bold;
}
.side{

    border-radius: 100%;
    background: white;
    height: 20px;
    width: 20px;
    margin-block-start: 1px;
    margin-inline-start: 5px;
    text-align: center;
    color: black;
} /* test */


.nav-right{
    float: right;
}
/*akhir menu*/

.clear{
    clear: both;
}
.badan{
    height: 100vh;
}
/*bagian sidebar*/
.wrap .badan .sidebar{
    background: rgb(255, 255, 255);
    float: left; 
    width: 17%;
    height: 100%;

    overflow-y: auto;
    /* position: fixed;
     */
    /* top: 0;
    left: 0; */
}
.wrap .badan .sidebar img{
  padding: 3px;
  margin: 3px;

  width: 220px;
}
.wrap .badan .sidebar button{
  /* margin: 30px; */
  margin-left: 30px;
  margin-bottom: 30px;
  margin-top: -2px;

  padding-left: 10px;
  padding-right: 20px;
 text-align: center;

  width: 175px;
  height: 40px;
  border-radius: 10px;
  border: none;
  background: #69CF9D;
  color: white;
  font-size: 14px;
}
.wrap .badan .sidebar button p{
    float: right;
    padding-top: 5px;
    padding-right: 10px;
}
.wrap .badan .sidebar button i{
    margin-right: 5px;
    
}

.wrap .badan .sidebar .menu{
            width: 100%;
            background: white;
            /* margin-top: 80px;  */
            margin-top: -15px;
        }
.wrap .badan .sidebar .menu .item{
            position: relative;
            cursor: pointer;
        }
.wrap .badan .sidebar .menu .item a{
            /* color: white; */
            /* background: #c68b8b; */
            font-size: 12px;
            /* text-decoration: none
            /* display: block; */
            padding: 3px 10px;
            line-height: 25px;
        }


        /* .wrap .badan .sidebar .menu .item a:hover{
            width: 100%;
            background: #69CF9D;
            transition: 0.3s ease;
        }tdk berguna di atas */
        

        
        .wrap .badan .sidebar .menu .item i{
            margin-right: 15px;
        }
        .wrap .badan .sidebar .menu .item a .dropdown{
            position: absolute;
            right: 35px;
            margin: 5px;
            transition: 0.3s ease;

        }
        
        .wrap .badan .sidebar .menu .item .sub-menu{
            /* background: rgba(255, 255, 255, 0.1); */
            display: none;
        }/* tdk berguna di atas */

        .wrap .badan .sidebar .menu .item .sub-menu a{
            padding-left: 40px;
            color: black;
            text-decoration: none;
        }
        .rotate{
            transform: rotate(90deg);
        }
/*akhir sidebar*/

.wrap .badan .content{
    background: #E8E8E8;
    float: left;
    height: 100%;
    width: 80%;  
}
.wrap .badan .content .tamp1{
    margin-left: 50px;
    margin-top: 20px;
    padding-left: 10px; 
    color: white;  
}
.wrap .badan .content .tamp1_1{
    height: 75px;
    width: 33%;
    float: left;
    background: #69CF9D;
    border: 1px #ada5a5;
    box-shadow: 0px 3px #ada5a5;
    border-right: 1px solid #ada5a5;
    
}
.wrap .badan .content .tamp1_1 i{
    float: left;
    margin-top: 15px;
    margin-left: 50px;
}
.wrap .badan .content .tamp1_1 p{
    float: left;
    margin-left: 35px;
    margin-top: 20px;
    font-size: 12px;
}
.wrap .badan .content .tamp1_1 .notif{
    margin-left: -90px;
    font-size: 14px;
    font-weight: bold;
    margin-top: 10px;
    
}

.wrap .badan .content .tamp1_2 i{
    float: left;
    margin-top: 15px;
    margin-left: 50px;
}
.wrap .badan .content .tamp1_2 p{
    float: left;
    margin-left: 35px;
    margin-top: 20px;
    font-size: 12px;
}
.wrap .badan .content .tamp1_2 .notif{
    margin-left: -110px;
    font-size: 14px;
    font-weight: bold;
    margin-top: 10px; 
}

.wrap .badan .content .tamp1_3 i{
    float: left;
    margin-top: 15px;
    margin-left: 50px;
}
.wrap .badan .content .tamp1_3 p{
    float: left;
    margin-left: 35px;
    margin-top: 20px;
    font-size: 12px;
}
.wrap .badan .content .tamp1_3 .notif{
    margin-left: -110px;
    font-size: 14px;
    font-weight: bold;
    margin-top: 10px;
    
}

.wrap .badan .content .tamp1_2{
    height: 75px;
    width: 33%;
    float: left;
    /* margin-left: 20px; */
    background: #69CF9D;
    border: 1px #ada5a5;
    box-shadow: 0px 3px #ada5a5;
    border-right: 1px solid #ada5a5;
}
.wrap .badan .content .tamp1_3{
    height: 75px;
    width: 33%;
    float: left;
    background: #69CF9D;
    border: 1px #ada5a5;
    box-shadow: 0px 3px #ada5a5;
}

.wrap .badan .content .tamp2{
    margin-left: 50px;
    margin-top: 115px;
    padding-left: 10px;  
}
.wrap .badan .content .tamp2_1{
    height: 65px;
    width: 47%;
    float: left;
    background: #ffffff;
    border: 1px solid #69CF9D;
    box-shadow: 0px 3px #ada5a5;
    
}
.wrap .badan .content .tamp2_2{
    margin-left: 50px;
    height: 65px;
    width: 47%;
    float: left;
    background: #ffffff;
    border: 1px solid #69CF9D;
    box-shadow: 0px 3px #ada5a5;   
}
.wrap .badan .content .tamp2_1 i{
    float: left;
    margin-top: 10px;
    margin-left: 60px;
}
.wrap .badan .content .tamp2_1 p{
    float: left;
    margin-left: 140px;
    margin-top: 7px;
    font-size: 16px;
}
.wrap .badan .content .tamp2_1 .notif1{
    margin-left: -45px;
    font-size: 18px;
    font-weight: bold;
    margin-top: 6px;
}
.wrap .badan .content .tamp2_2 i{
    float: left;
    margin-top: 10px;
    margin-left: 60px;
}
.wrap .badan .content .tamp2_2 p{
    float: left;
    margin-left: 140px;
    margin-top: 7px;
    font-size: 16px;
}
.wrap .badan .content .tamp2_2 .notif1{
    margin-left: -60px;
    font-size: 18px;
    font-weight: bold;
    margin-top: 6px;
}
.wrap .badan .content .tamp3{
    margin-left: 60px;
    margin-top: 200px;
    padding-left: 10px;
    height: 100vh;
    width: 93%;  
    background: white;
    border-radius: 10px;
}
.wrap .badan .content .tamp3 .jud{
    padding-top: 15px;
    padding-left: 15px;
    padding-bottom: 8px;
    font-size: 12px;
    color: #9E9E9E;
    font-weight: bold;
}
.wrap .badan .content .tamp3 .garis_horizontal{
    border-top: 2px #69CF9D solid;
    height: 0px;
    width: 99%;
}
.wrap .badan .content .tamp3 .ket1{
    padding-left: 20px;
    padding-top: 10px;
    font-size: 14px;
    color: #9E9E9E;
    font-weight: bold;
}

.wrap .badan .content .tamp3 .tamp3_1{
    margin-left: 20px;
    margin-top: -40px;
}

.wrap .badan .content .tamp3 .tabel1 {
        color: #D9D9D9;
        border-collapse: collapse;
        width: 96%;
        border: 2px solid #D9D9D9;
        margin-top: 50px;

    }
    
    .wrap .badan .content .tamp3 .tabel1 tr th {
        background: #E8EFFF;
        color: #9E9E9E;
        font-weight: bold;
        font-size: 14px;
        border-left: 2px solid #D9D9D9;
    }
    
    .wrap .badan .content .tamp3 .tabel1, th, td {
        padding: 8px 20px;
        text-align: center;
        border-left: 2px solid #D9D9D9;
    }
    
    .wrap .badan .content .tamp3 .tabel1 tr:nth-child(odd) {
        background: #F6F6F6;
    }

.wrap .badan .content .tampilan-dashboard{
    /* padding: 20px; */
    padding-top: 15px;
    margin: 10px;
    height: 35px;
    width: 170px;
    background: white;
    text-align: center;
    border-radius: 20px;
    font-size: 14px;
    font-weight: bold;
    
}







/* .wrap .footer{
    width: 100%;
    padding: 10px;
    align-items: center;
} */
  </style>
</head>
<body>
  <div class="wrap">
    {{-- <div class="header">          
        <h1>reza blog</h1>
        <p>Tutorial belajar membuat layout website sederhana</p>
    </div> --}}
    <div class="menu">
        <ul>
            <li><a href="#">
                <div class="side">
                    <i class='fas fa-angle-left drop'>
                    </i>
                </div>
                </a>
            </li>
            <li><a href="#">Gudang</a></li>

            <div class="nav-right">
            <li><a href=""><i class='bx bxs-bell'></i></a></li>
            <li><a href="/logout"><i class='bx bxs-user-circle'></i></a></li>
            </div>

        </ul>
    </div>


    <div class="badan">    

        <div class="sidebar">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU" alt="">
           <a href="/"><button>
            <i class='bx bxs-cabinet fa-2x'></i><p>Dashboard</p>
            </button>
        </a>

{{-- sidebar navigasi --}}
<div class="menu">
    {{-- @if (Auth::user()->role == 'admin') --}}
    <div class="item">
        <a class="sub-btn"><i class='bx bx-file'></i>GUDANG<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="/permintaan_baru" class="sub-item"><i class='bx bx-notepad'></i>Permintaan Baru</a>
        </div>
    </div>
    <div class="item">
        <a class="sub-btn"><i class='bx bx-wrench'></i>BARANG<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="/stock_barang" class="sub-item"><i class='bx bx-notepad'></i>Stock Barang</a>
            <br>
            <a href="/stock_barang_masuk" class="sub-item"><i class='bx bx-notepad'></i>Data Barang Masuk</a>
            <br>
            <a href="/stock_barang_keluar" class="sub-item"><i class='bx bx-notepad'></i>Data Barang Keluar</a>
        </div>


    </div>
    {{-- @endif --}}

   
    {{-- @if (Auth::user()->role == 'inventory')
    <div class="item">
        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
        </div>
    </div>
    @endif
   

    @if (Auth::user()->role == 'finance')
    <div class="item">
        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan Keuangan</a>
        </div>
    </div>
    @endif --}}

</div>

        </div>


        <div class="content">
            <div class="tampilan-dashboard">
                Dashboard
            </div>   
            
            <div class="tamp1">
                <div class="tamp1_1">
                    <i class='bx bx-package fa-3x''></i>
                    <p>Permintaan Baru</p>
                    <br>
                    <p class="notif">10</p>
                </div>
                <div class="tamp1_2">
                    <i class='bx bx-archive-in fa-3x'></i>
                    <p>Data Barang Masuk</p>
                    <br>
                    <p class="notif">13</p>
                </div>
                <div class="tamp1_3">
                    <i class='bx bx-archive-out fa-3x'></i>
                    <p>Data Barang Keluar</p>
                    <br>
                    <p class="notif">08</p>
                </div>
            </div>

            <div class="tamp2">
                <div class="tamp2_1">
                    <i class='bx bx-file fa-3x'></i>
                    <p>Kategori</p>
                    <br>
                    <p class="notif1">34</p>
                </div>
                <div class="tamp2_2">
                    <i class='bx bx-folder-open fa-3x'></i>
                    <p>Data Satuan</p>
                    <br>
                    <p class="notif1">4</p>
                    
                </div>
            </div>

            <div class="tamp3">
                <div class="jud">Stock mencapai batas maksimum</div>
                <div class="garis_horizontal"></div>
                <div class="ket1">
                    Tampilkan 10 data
                </div>
                <div class="tamp3_1">
                    <table class="tabel1">
                        <tr>
                                <th>No</th>
                                <th>ID Barang</th>
                                <th>Nama Barang</th>
                                <th>Kategori</th>
                                <th>Quantity</th>
                                <th>Satuan</th>
                                
                                </tr>
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                
                                </tr>
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                
                                </tr>
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                
                                </tr>
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                
                                </tr>
                        <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                                
                                </tr>
                        </table>
                </div>
            </div>

        </div>
    </div>
    {{-- <div class="clear"></div>
    <div class="footer">
        @JnetSurfer - JongJavaTechnology
    </div> --}}
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('.sub-btn').click(function(){
            $(this).next('.sub-menu').slideToggle();
            $(this).find('.dropdown').toggleClass('rotate');
        });
    });
</script>

</body>
</html>