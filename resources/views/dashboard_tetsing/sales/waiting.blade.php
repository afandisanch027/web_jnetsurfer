<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Awinet | Admin</title>
  <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">



  
  </head>


  <style type="text/css">
    *{
        font-family: 'Poppins', sans-serif;
      padding: 0;
      margin: 0;
    }
    .wrap{
    background: #E8E8E8;
    width: 100%;
    height: 100vh;
    margin: 0px;
    padding: 0px;
    font-family: po

}
    

/* bagian header
.wrap .header{
    background: green;
    /*height: 50px;*/
    /* padding: 2px 10px;
} */

/*akhir header*/


/*bagian menu*/
.wrap .menu{
    background: #69CF9D;
    
    
}

.wrap .menu ul{
    padding: 0;
    margin: 0;
    
    overflow: hidden;
}

.wrap .menu ul li{
    float: left;
    list-style-type: none;
    padding: 10px;
    
}
.wrap .menu ul li a{
    font-size: 18px;
    color: white;
    text-decoration: none;
    font-weight: bold;

}
.side{

    border-radius: 100%;
    background: white;
    height: 20px;
    width: 20px;
    margin-block-start: 1px;
    margin-inline-start: 5px;
    text-align: center;
    color: black
} /* test */


.nav-right{
    float: right;
}
/*akhir menu*/

.clear{
    clear: both;
}
.badan{
    height: 100vh;
}
/*bagian sidebar*/
.wrap .badan .sidebar{
    background: rgb(255, 255, 255);
    float: left; 
    width: 17%;
    height: 100%;

    overflow-y: auto;
    /* position: fixed;
     */
    /* top: 0;
    left: 0; */
}
.wrap .badan .sidebar img{
  padding: 3px;
  margin: 3px;

  width: 220px;
}
.wrap .badan .sidebar button{
  /* margin: 30px; */
  margin-left: 30px;
  margin-bottom: 30px;
  margin-top: -2px;

  padding-left: 10px;
  padding-right: 20px;
 text-align: center;

  width: 175px;
  height: 40px;
  border-radius: 10px;
  border: none;
  background: #69CF9D;
  color: white;
  font-size: 14px;
}
.wrap .badan .sidebar button p{
    float: right;
    padding-top: 5px;
    padding-right: 10px;
}
.wrap .badan .sidebar button i{
    margin-right: 5px;
    
}

.wrap .badan .sidebar .menu{
            width: 100%;
            background: white;
            /* margin-top: 80px;  */
            margin-top: -15px;
        }
.wrap .badan .sidebar .menu .item{
            position: relative;
            cursor: pointer;
        }
.wrap .badan .sidebar .menu .item a{
            /* color: white; */
            /* background: #c68b8b; */
            font-size: 12px;
            /* text-decoration: none
            /* display: block; */
            padding: 3px 10px;
            line-height: 25px;
        }


        /* .wrap .badan .sidebar .menu .item a:hover{
            width: 100%;
            background: #69CF9D;
            transition: 0.3s ease;
        }tdk berguna di atas */
        

        
        .wrap .badan .sidebar .menu .item i{
            margin-right: 15px;
        }
        .wrap .badan .sidebar .menu .item a .dropdown{
            position: absolute;
            right: 35px;
            margin: 5px;
            transition: 0.3s ease;

        }
        
        .wrap .badan .sidebar .menu .item .sub-menu{
            /* background: rgba(255, 255, 255, 0.1); */
            display: none;
        }/* tdk berguna di atas */

        .wrap .badan .sidebar .menu .item .sub-menu a{
            padding-left: 40px;
            color: black;
            text-decoration: none;
        }
        .rotate{
            transform: rotate(90deg);
        }
/*akhir sidebar*/

.wrap .badan .content{
    background: #E8E8E8;
    float: left;
    height: 100%;
    width: 80%; 
    
}
.wrap .badan .content .tampilan-dashboard{
    /* padding: 20px; */
    padding-top: 10px;
    margin: 10px;
    height: 35px;
    width: 170px;
    background: white;
    text-align: center;
    border-radius: 20px;
    font-size: 18px;
    font-weight: bold; 
}

.wrap .badan .content .tampilan-isi{
    background: white;
    width: 98%;
    height: 80%;
    margin-left: 30px;
    margin-top: 20px;
    border-radius: 20px;
}
.wrap .badan .content .tampilan-isi form{
    margin-left: 50px;
    padding-block-start: 5px;
    font-size: 14px;
}
.wrap .badan .content .tampilan-isi form .judul{
    color: #9E9E9E;
    padding: 3px;
    margin-top: 10px;
}
.wrap .badan .content .tampilan-isi form .keterangan input{
    border: 2px solid #69CF9D;
    height: 25px;
    width: 80%;
    border-radius: 10px;
    padding: 2px;
    margin-top: 3px;
}
.wrap .badan .content .tampilan-isi form .keterangan select{
    border: 2px solid #69CF9D;
    height: 35px;
    width: 81%;
    border-radius: 10px;
    padding: 2px;
    margin-top: 3px;
}
.wrap .badan .content .tampilan-isi form button{
    float: right;
    margin-right: 200px;
    margin-top: 10px;
    height: 40px;
    width: 100px;
    border-radius: 10px;
    background: #69CF9D;
    border: none;
    color: white;

}



/* .wrap .footer{
    width: 100%;
    padding: 10px;
    align-items: center;
} */
  </style>
</head>
<body>
  <div class="wrap">
    {{-- <div class="header">          
        <h1>reza blog</h1>
        <p>Tutorial belajar membuat layout website sederhana</p>
    </div> --}}
    <div class="menu">
        <ul>
            <li><a href="#">
                <div class="side">
                    <i class='fas fa-angle-left drop'>
                    </i>
                </div>
                </a>
            </li>
            <li><a href="#">Admin</a></li>

            <div class="nav-right">
            <li><a href=""><i class='bx bxs-bell'></i></a></li>
            <li><a href="/logout"><i class='bx bxs-user-circle'></i></a></li>
            </div>

        </ul>
    </div>


    <div class="badan">    

        <div class="sidebar">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU" alt="">
           <a href="/"><button>
            <i class='bx bxs-cabinet fa-2x'></i><p>Dashboard</p>
            </button>
        </a>

{{-- sidebar navigasi --}}
<div class="menu">
    {{-- @if (Auth::user()->role == 'admin') --}}
    <div class="item">
        <a class="sub-btn"><i class='bx bx-file'></i>SALES<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="/survei" class="sub-item"><i class='bx bx-notepad'></i>Survei Baru</a>
        </div>
    </div>
    <div class="item">
        <a class="sub-btn"><i class='bx bx-wrench'></i>TEKNISI<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="/hasil_survei" class="sub-item"><i class='bx bx-notepad'></i>Pemintaan Baru</a>
        </div>
    </div>
    <div class="item">
        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
        </div>
    </div>
    <div class="item">
        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan Keuangan</a>
        </div>
    </div>
    {{-- @endif --}}

{{--    
    @if (Auth::user()->role == 'inventory')
    <div class="item">
        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
        </div>
    </div>
    @endif
   

    @if (Auth::user()->role == 'finance')
    <div class="item">
        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i class="fas fa-angle-right dropdown"></i></a>
        <div class="sub-menu">
            <a href="#" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan Keuangan</a>
        </div>
    </div>
    @endif --}}

</div>

        </div>


        <div class="content">
            <div class="tampilan-dashboard">
                Jadwal Survei
            </div>

            <div class="tampilan-isi">
                
                <form action="{{ route('submit.jadwal_survei') }}" method="POST">
                    @csrf
                    <input type="hidden" name="sales_id" value="{{ $sales->id }}">
                    <input type="hidden" name="status" value="waiting_survei">
                    <div class="judul">
                        <label for="nama_pelanggan">Nama Pelanggan</label>
                    </div>
                    <div class="keterangan">
                        <input type="text" id="name" name="name" value="{{ $sales->plgName }}" readonly>
                    </div>
                    <div class="judul">
                        <label for="alamat">Alamat</label>
                    </div>
                    <div class="keterangan">
                    <input type="text"value="{{ $sales->alamat }}" readonly>
                    </div>
                    <div class="judul">
                    <label for="no_ticket">Nomer Ticket</label>
                    </div> 
                    <div class="keterangan">
                    <input type="text" value="{{ $sales->noTicket }}" readonly>
                    </div>
                    <div class="judul">
                        <label for="no_tlp">No Telephone</label>
                    </div>
                    <div class="keterangan">
                        <input type="text" id="no_tlp" name="no_tlp" value="{{ $sales->noHp }}" readonly>
                    </div>
                    <div class="judul">
                    <label for="tanggal_survei">Tanggal Survei *</label>
                    </div>
                    <div class="keterangan">
                    <input type="date" name="tanggal_survei" id="date_timepicker_end">
                    </div>
                    <div class="judul">
                    <label for="Team Teknisi">Team Teknisi *</label>
                    </div>

                    <div class="keterangan">
                    <select id="team" name="team">
                      <option value="team_1">Team 1</option>
                      <option value="team_2">Team 2</option>
                      <option value="team_3">Team 3</option>
                    </select>
                    </div>

                    <button type="submit">Terima</button>
                    
                </form>

         
                


                
            </div>


        </div>
    </div>
    {{-- <div class="clear"></div>
    <div class="footer">
        @JnetSurfer - JongJavaTechnology
    </div> --}}
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('.sub-btn').click(function(){
            $(this).next('.sub-menu').slideToggle();
            $(this).find('.dropdown').toggleClass('rotate');
        });
    });
</script>

</body>
</html>