<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Awinet | Admin</title>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>

<body>
    <div class="wrap">
        <div class="menu">

            <ul>
                <li><a href="#">
                        <div class="side">
                            <i class='fas fa-angle-left drop'>
                            </i>
                        </div>
                    </a>
                </li>
                <li><a href="#">Admin</a></li>

                <div class="nav-right">
                    <li><a href=""><i class='bx bxs-bell'></i></a></li>
                    <li><a href="
                {{ route('logout') }}
                "><i
                                class='bx bxs-user-circle'></i></a></li>
                </div>

            </ul>
        </div>


        <div class="badan">

            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>

                {{-- sidebar navigasi --}}
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-file'></i>SALES<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('sales.index') }}" class="sub-item"><i class='bx bx-notepad'></i>Survei
                                Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>TEKNISI<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="{{ route('teknisi.index') }}" class="sub-item"><i class='bx bx-notepad'></i>Pemintaan Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="/admin_gudang" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="/admin_finance" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan
                                Keuangan</a>
                        </div>
                    </div>
                    <div cl0o00ass="item">
                        <a class="sub-btn"><i class='bx bx-cog'></i>Settings<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="/account" class="sub-item"><i class='bx bxs-user-account'></i></i>Account</a>
                        </div>
                    </div>
                </div>

            </div>


            <div class="content">


                <div class="tampilan-dashboard">

                    Dashboard
                </div>

                <div class="tampilan-dashboard1">


                    <div class="tampilan-dash1">
                        <div class="dash-icons">
                            <i class='bx bx-file fa-4x'></i>
                        </div>
                        <div class="dash-keterangan">
                            Sales
                        </div>
                        <div class="dash-notif">
                            3
                        </div>
                    </div>

                    <div class="tampilan-dash2">
                        <div class="dash-icons">
                            <i class='bx bxs-box  fa-4x'></i>
                        </div>
                        <div class="dash-keterangan">
                            Inventory
                        </div>
                        <div class="dash-notif">
                            5
                        </div>
                    </div>
                </div>

                <div class="tampilan-dashboard2">

                    <div class="tampilan-dash3">
                        <div class="dash-icons">
                            <i class='bx bx-wrench  fa-4x'></i>
                        </div>
                        <div class="dash-keterangan">
                            Teknisi
                        </div>
                        <div class="dash-notif">
                            10
                        </div>
                    </div>

                    <div class="tampilan-dash4">
                        <div class="dash-icons">
                            <i class='bx bx-dollar-circle  fa-4x'></i>
                        </div>
                        <div class="dash-keterangan">
                            Finance
                        </div>
                        <div class="dash-notif">
                            10
                        </div>
                    </div>
                </div>

                <div class="diagram"><img src="https://www.jojonomic.com/wp-content/uploads/2020/12/11-1.jpg"
                        alt=""></div>

            </div>
        </div>
        {{-- <div class="clear"></div>
    <div class="footer">
        @JnetSurfer - JongJavaTechnology
    </div> --}}
    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.sub-btn').click(function() {
                $(this).next('.sub-menu').slideToggle();
                $(this).find('.dropdown').toggleClass('rotate');
            });
        });
    </script>

</body>

</html>
