<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Awinet</title>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>

<body>
    {{-- <div class="wrap"> --}}
        {{-- menu admin --}}
        {{-- @can('view_dashboard_admin')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/">Admin</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a href="
                    {{ route('logout') }}
                    "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan --}}

        {{-- menu inventory --}}
        {{-- @can('view_dashboard_inventory')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/">Gudang</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a href="
                            {{ route('logout') }}
                            "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan --}}

        {{-- menu Finance --}}
        {{-- @can('view_dashboard_finance')
            <div class="menu">
                <ul>
                    <li><a href="#">
                            <div class="side">
                                <i class='fas fa-angle-left drop'>
                                </i>
                            </div>
                        </a>
                    </li>
                    <li><a href="/">Finance</a></li>

                    <div class="nav-right">
                        <li><a href=""><i class='bx bxs-bell'></i></a></li>
                        <li><a
                                href="
                                            {{ route('logout') }}
                                            "><i
                                    class='bx bxs-user-circle'></i></a></li>
                    </div>
                </ul>
            </div>
        @endcan --}}

        <div class="badan">
            {{-- awal sidebar --}}

            {{-- sidebar_admin --}}
            @can('view_dashboard_admin')
                <div class="sidebar">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                        alt="">
                    <a href="/"><button>
                            <i class='bx bxs-cabinet fa-2x'></i>
                            <p>Dashboard</p>
                        </button>
                    </a>
                    <div class="menu">
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-file'></i>SALES<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/survei" class="sub-item"><i class='bx bx-notepad'></i>Survei Baru</a>
                            </div>
                        </div>
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-wrench'></i>TEKNISI<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/hasil_survei" class="sub-item"><i class='bx bx-notepad'></i>Pemintaan Baru</a>
                            </div>
                        </div>
                        <div class="item">
                            <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/admin_gudang" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
                            </div>
                        </div>
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/admin_finance" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan
                                    Keuangan</a>
                            </div>
                        </div>
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-cog'></i>SETTINGS<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/account" class="sub-item"><i class='bx bxs-user-account'></i></i>Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            {{-- sidebar_finance --}}
            @can('view_dashboard_finance')
                <div class="sidebar">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                        alt="">
                    <a href="/"><button>
                            <i class='bx bxs-cabinet fa-2x'></i>
                            <p>Dashboard</p>
                        </button>
                    </a>
                    <div class="menu">
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-wrench'></i>FINANCE<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/data_pemasukan" class="sub-item"><i class='bx bx-notepad'></i>Data
                                    Pemasukan</a>
                                <br>
                                <a href="/data_pengeluaran" class="sub-item"><i class='bx bx-notepad'></i>Data
                                    Pengeluaran</a>
                                <br>
                                <a href="/laporan_keuangan" class="sub-item"><i class='bx bx-notepad'></i>Laporan
                                    Keuangan</a>
                                <br>
                                <a href="/komisi" class="sub-item"><i class='bx bx-notepad'></i>Komisi</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan

            {{-- sidebar_Gudang --}}
            @can('view_dashboard_inventory')
                <div class="sidebar">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                        alt="">
                    <a href="/"><button>
                            <i class='bx bxs-cabinet fa-2x'></i>
                            <p>Dashboard</p>
                        </button>
                    </a>
                    <div class="menu">
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-file'></i>GUDANG<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/permintaan_baru" class="sub-item"><i class='bx bx-notepad'></i>Permintaan
                                    Baru</a>
                            </div>
                        </div>
                        <div class="item">
                            <a class="sub-btn"><i class='bx bx-wrench'></i>BARANG<i
                                    class="fas fa-angle-right dropdown"></i></a>
                            <div class="sub-menu">
                                <a href="/stock_barang" class="sub-item"><i class='bx bx-notepad'></i>Stock Barang</a>
                                <br>
                                <a href="/stock_barang_masuk" class="sub-item"><i class='bx bx-notepad'></i>Data Barang
                                    Masuk</a>
                                <br>
                                <a href="/stock_barang_keluar" class="sub-item"><i class='bx bx-notepad'></i>Data Barang
                                    Keluar</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endcan
            {{-- endsidebar --}}

            {{-- isi --}}
            {{-- isi_admin --}}
            @can('view_dashboard_admin')
                <div class="content">
                    <div class="tampilan-dashboard">
                        Dashboard
                    </div>
                    <div class="tampilan-dashboard1">
                        <div class="tampilan-dash1">
                            <div class="dash-icons">
                                <i class='bx bx-file fa-4x'></i>
                            </div>
                            <div class="dash-keterangan">
                                Sales
                            </div>
                            <div class="dash-notif">
                                3
                            </div>
                        </div>
                        <div class="tampilan-dash2">
                            <div class="dash-icons">
                                <i class='bx bxs-box  fa-4x'></i>
                            </div>
                            <div class="dash-keterangan">
                                Inventory
                            </div>
                            <div class="dash-notif">
                                5
                            </div>
                        </div>
                    </div>
                    <div class="tampilan-dashboard2">
                        <div class="tampilan-dash3">
                            <div class="dash-icons">
                                <i class='bx bx-wrench  fa-4x'></i>
                            </div>
                            <div class="dash-keterangan">
                                Teknisi
                            </div>
                            <div class="dash-notif">
                                10
                            </div>
                        </div>
                        <div class="tampilan-dash4">
                            <div class="dash-icons">
                                <i class='bx bx-dollar-circle  fa-4x'></i>
                            </div>
                            <div class="dash-keterangan">
                                Finance
                            </div>
                            <div class="dash-notif">
                                10
                            </div>
                        </div>
                    </div>
                    <div class="diagram"><img src="https://www.jojonomic.com/wp-content/uploads/2020/12/11-1.jpg"
                            alt=""></div>
                </div>
            @endcan

            {{-- isi_finance --}}
            @can('view_dashboard_finance')
                <div class="content">
                    <div class="tampilan-dashboard">
                        Dashboard
                    </div>
                    <div class="tampil1">
                        <p class="tampil1_1">Total Income</p>
                        <br>
                        <p class="tampil1_2">Rp. 2.500.00,-</p>
                    </div>
                    <div class="tampil2">
                        <p class="tampil2_3">Total Expenses</p>
                        <br>
                        <p class="tampil2_4">Rp. 10.500.00,-</p>
                    </div>
                    <img src="https://wikielektronika.com/wp-content/uploads/2022/01/diagram-garis-tunggal.jpg"
                        alt="">
                </div>
            @endcan

            {{-- isi_teknisi --}}
            @can('view_dashboard_inventory')
                <div class="content">
                    <div class="tampilan-dashboard">
                        Dashboard
                    </div>

                    <div class="tamp1">
                        <div class="tamp1_1">
                            <i class='bx bx-package fa-3x''></i>
                            <p>Permintaan Baru</p>
                            <br>
                            <p class="notif">10</p>
                        </div>
                        <div class="tamp1_2">
                            <i class='bx bx-archive-in fa-3x'></i>
                            <p>Data Barang Masuk</p>
                            <br>
                            <p class="notif">13</p>
                        </div>
                        <div class="tamp1_3">
                            <i class='bx bx-archive-out fa-3x'></i>
                            <p>Data Barang Keluar</p>
                            <br>
                            <p class="notif">08</p>
                        </div>
                    </div>

                    <div class="tamp2">
                        <div class="tamp2_1">
                            <i class='bx bx-file fa-3x'></i>
                            <p>Kategori</p>
                            <br>
                            <p class="notif1">34</p>
                        </div>
                        <div class="tamp2_2">
                            <i class='bx bx-folder-open fa-3x'></i>
                            <p>Data Satuan</p>
                            <br>
                            <p class="notif1">4</p>

                        </div>
                    </div>

                    <div class="tamp3">
                        <div class="jud">Stock mencapai batas maksimum</div>
                        <div class="garis_horizontal"></div>
                        <div class="ket1">
                            Tampilkan 10 data
                        </div>
                        <div class="tamp3_1">
                            <table class="tabel1">
                                <tr>
                                    <th>No</th>
                                    <th>ID Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Kategori</th>
                                    <th>Quantity</th>
                                    <th>Satuan</th>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>


                                </tr>
                            </table>
                        </div>
                    </div>

                </div>
            @endcan

            {{-- endisi --}}
        </div>

    {{-- </div> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('.sub-btn').click(function() {
                $(this).next('.sub-menu').slideToggle();
                $(this).find('.dropdown').toggleClass('rotate');
            });
        });
    </script>
</body>

</html>
