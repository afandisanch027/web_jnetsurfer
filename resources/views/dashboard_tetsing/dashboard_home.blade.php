<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Awinet</title>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap"
        rel="stylesheet">

    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">

</head>

<body>
    {{-- <div class="wrap"> --}}
    {{-- menu admin --}}
    @can('view_dashboard_admin')
        <div class="menu">
            <ul>
                <li><a href="#">
                        <div class="side">
                            <i class='fas fa-angle-left drop'>
                            </i>
                        </div>
                    </a>
                </li>
                <li><a href="/">Admin</a></li>

                <div class="nav-right">
                    <li><a href=""><i class='bx bxs-bell'></i></a></li>
                    <li><a href="
                    {{ route('logout') }}
                    "><i
                                class='bx bxs-user-circle'></i></a></li>
                </div>
            </ul>
        </div>
    @endcan

    {{-- menu inventory --}}
    @can('view_dashboard_inventory')
        <div class="menu">
            <ul>
                <li><a href="#">
                        <div class="side">
                            <i class='fas fa-angle-left drop'>
                            </i>
                        </div>
                    </a>
                </li>
                <li><a href="/">Gudang</a></li>

                <div class="nav-right">
                    <li><a href=""><i class='bx bxs-bell'></i></a></li>
                    <li><a href="
                            {{ route('logout') }}
                            "><i
                                class='bx bxs-user-circle'></i></a></li>
                </div>
            </ul>
        </div>
    @endcan

    {{-- menu Finance --}}
    @can('view_dashboard_finance')
        <div class="menu">
            <ul>
                <li><a href="#">
                        <div class="side">
                            <i class='fas fa-angle-left drop'>
                            </i>
                        </div>
                    </a>
                </li>
                <li><a href="/">Finance</a></li>

                <div class="nav-right">
                    <li><a href=""><i class='bx bxs-bell'></i></a></li>
                    <li><a
                            href="
                                            {{ route('logout') }}
                                            "><i
                                class='bx bxs-user-circle'></i></a></li>
                </div>
            </ul>
        </div>
    @endcan

    <div class="badan">
        {{-- awal sidebar --}}

        {{-- sidebar_admin --}}
        @can('view_dashboard_admin')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-file'></i>SALES<i class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="/konten1" class="sub-item"><i class='bx bx-notepad'></i>Survei Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>TEKNISI<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="/konten2" class="sub-item"><i class='bx bx-notepad'></i>Pemintaan Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bxs-box'></i>INVENTORY<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bx-package'></i>Stock Gudang</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-dollar-circle'></i>FINANCE<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bx-credit-card-front'></i>Laporan
                                Keuangan</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-cog'></i>SETTINGS<i class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bxs-user-account'></i></i>Account</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        {{-- sidebar_finance --}}
        @can('view_dashboard_finance')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>FINANCE<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Data
                                Pemasukan</a>
                            <br>
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Data
                                Pengeluaran</a>
                            <br>
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Laporan
                                Keuangan</a>
                            <br>
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Komisi</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan

        {{-- sidebar_Gudang --}}
        @can('view_dashboard_inventory')
            <div class="sidebar">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
                <a href="/"><button>
                        <i class='bx bxs-cabinet fa-2x'></i>
                        <p>Dashboard</p>
                    </button>
                </a>
                <div class="menu">
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-file'></i>GUDANG<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Permintaan
                                Baru</a>
                        </div>
                    </div>
                    <div class="item">
                        <a class="sub-btn"><i class='bx bx-wrench'></i>BARANG<i
                                class="fas fa-angle-right dropdown"></i></a>
                        <div class="sub-menu">
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Stock Barang</a>
                            <br>
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Data Barang
                                Masuk</a>
                            <br>
                            <a href="" class="sub-item"><i class='bx bx-notepad'></i>Data Barang
                                Keluar</a>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
        {{-- endsidebar --}}

        {{-- isi --}}
        <div class="content">
            <div class="tampilan-dashboard">
                Dashboard
            </div>
        {{-- isi_admin --}}


        {{-- endisi --}}
    </div>

    {{-- </div> --}}

    <script type="text/javascript">
        $(document).ready(function() {
            $('.sub-btn').click(function() {
                $(this).next('.sub-menu').slideToggle();
                $(this).find('.dropdown').toggleClass('rotate');
            });
        });

    </script>
</body>

</html>
