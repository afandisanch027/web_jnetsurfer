@extends('layouts.app')

@section('title', 'Jadwal Survei')

@section('content')
<div class="tampilan-dashboard">
                Jadwal Survei
            </div>

            <div class="tampilan-isi">
                
                <form action="{{ route('submit.jadwal_survei') }}" method="POST">
                    @csrf
                    <input type="hidden" name="sales_id" value="{{ $sales->id }}">
                    <input type="hidden" name="status" value="waiting_survei">
                    <div class="judul">
                        <label for="nama_pelanggan">Nama Pelanggan</label>
                    </div>
                    <div class="keterangan">
                        <input type="text" id="name" name="name" value="{{ $sales->plgName }}" readonly>
                    </div>
                    <div class="judul">
                        <label for="alamat">Alamat</label>
                    </div>
                    <div class="keterangan">
                    {{-- <input type="text"value="{{ $sales->alamat }}" readonly> --}}
                    <input type="text" value="Jalan {{ $sales->jalan }}, RT {{ $sales->rt }}, RW {{ $sales->rw }}, Kelurahan/Desa {{ $sales->kelurahan_desa }}, Kecamatan {{ $sales->kecamatan }}, Kabupaten {{ $sales->kabupaten }}" readonly>
                    </div>
                    <div class="judul">
                    <label for="no_ticket">Nomer Ticket</label>
                    </div> 
                    <div class="keterangan">
                    <input type="text" value="{{ $sales->noTicket }}" readonly>
                    </div>
                    <div class="judul">
                        <label for="no_tlp">No Telephone</label>
                    </div>
                    <div class="keterangan">
                        <input type="text" id="no_tlp" name="no_tlp" value="{{ $sales->noHp }}" readonly>
                    </div>
                    <div class="judul">
                    <label for="tanggal_survei">Tanggal Survei *</label>
                    </div>
                    <div class="keterangan">
                    <input type="date" name="tanggal_survei" id="date_timepicker_end">
                    </div>
                    <div class="judul">
                    <label for="Team Teknisi">Team Teknisi *</label>
                    </div>

                    <div class="keterangan">
                    <select id="team" name="team">
                      <option value="team_1">Team 1</option>
                      <option value="team_2">Team 2</option>
                      <option value="team_3">Team 3</option>
                    </select>
                    </div>

                    <button type="submit">Terima</button>
                    
                </form>      
            </div>
@endsection
