@extends('layouts.app')

@section('title', 'Survei')

@section('content')
        <div class="tampilan-dashboard">
            Survei
        </div>
        <div class="tampilan-isi">
                <select id="statusFilter" onchange="filterByStatus()" class="filter">
                    <option value="all">Filter</option>
                    <option value="Waiting">Waiting</option>
                    <option value="Selesai">Selesai</option>
                </select>
            <div class="search">
                <input type="text" id="searchInput" placeholder="Cari">
                <button onclick="search()"><i class='bx bx-search'></i></button>
            </div>

            <table>
                <thead>
                    <tr>
                        <th class="corner-awal">No Ticket</th>
                        <th>Nama Pelanggan</th>
                        <th>No. Telp</th>
                        <th>Paket</th>
                        <th>Harga</th>
                        <th>Status</th>
                        <th class="corner-end">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sales as $sale)
                        <tr>
                            <td class="corner-awal">{{ $sale->noTicket }}</td>
                            <td>{{ $sale->plgName }}</td>
                            <td>{{ $sale->noHp }}</td>
                            <td>{{ $sale->paket }}</td>
                            <td>{{ $sale->harga }}</td>
                            <td>{{ $sale->status }}</td>
                            <td class="corner-end">
                                @if ($sale->status == 'Waiting')
                                    <a href="{{ url('form_jadwal_survei/' . $sale->id) }}"><i class='bx bxs-edit'></i></a>
                                @elseif ($sale->status == 'Selesai')
                                    <a href="#"><i class='bx bxs-edit'></i></a>
                                @else
                                    {{ $sale->status }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
            <div class="page">
            {{ $sales->links() }}
            </div>
        </div>
@endsection
