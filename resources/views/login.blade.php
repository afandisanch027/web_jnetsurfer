<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Awinet | Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">

    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }

        body {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background: #69cf9d;
            margin: 0;
        }

        .container-login {
            width: 100%;
            height: 100vh;
            display: flex;
            flex-direction: column;
            max-width: 85%;
            background: #ffffff;
            border-radius: 20px;
            box-shadow: 0 10px 15px rgba(0, 0, 0, 0.1);
            padding: 20px;
        }

        .login {
            width: 100%;
            display: flex;
            flex-direction: column;
        }

        .right img {
            width: 100%;
            height: auto;
            border-top-right-radius: 15px;
            border-bottom-right-radius: 15px;
            object-fit: cover;
            margin-bottom: 20px; /* Jarak antara gambar dan form login */
        }

        h1 {
            margin: 20px;
            text-align: center;
            font-weight: bolder;
            text-transform: uppercase;
            font-size: 3vw;
        }

        form {
            width: 80%;
            margin: 20px auto;
            z-index: 1; /* Memastikan form tetap di atas gambar */
            position: relative; /* Menetapkan posisi relatif */
        }

        p {
            text-align: end;
            margin: 10px;
            margin-inline-end: 8%;
        }

        a {
            color: #B9B7B7;
        }

        form label {
            display: block;
            font-size: 16px;
            font-weight: 600;
            padding: 5px;
        }

        input {
            width: 85%;
            height: 5vh;
            margin: 10px;
            border: none;
            outline: none;
            padding: 8px;
            border-radius: 20px;
            background-color: #D9D9D9;
            margin-inline-start: 10%;
            font-size: 1.5vw;
        }

        button {
            margin-inline-start: 10%;
            border: none;
            outline: none;
            padding: 8px;
            width: 85%;
            height: 6vh;
            color: #ffffff;
            font-size: 2vw;
            cursor: pointer;
            margin-top: 20px;
            border-radius: 18px;
            background: #FF8A48;
        }

        button:hover {
            background: rgb(105, 207, 157)
        }

        @media (max-width: 880px) {
            .container-login {
                width: 100%;
                max-width: 750px;
                margin-left: 0;
                margin-right: 0;
            }

            form {
                width: 100%;
                margin: 20px auto;
            }

            .login {
                width: 100%;
                padding: 20px;
                flex-direction: column;
            }

            button {
                width: 100%;
            }

            .loading {
                display: none;
                width: 20px;
                height: 20px;
                border: 2px solid rgba(0, 0, 0, 0.3);
                border-top: 2px solid #000000;
                border-radius: 50%;
                animation: spin 1s linear infinite;
                position: absolute;
                right: 10px;
                top: 50%;
                transform: translateY(-50%);
            }

            @keyframes spin {
                0% {
                    transform: rotate(0deg);
                }

                100% {
                    transform: rotate(360deg);
                }
            }
        }
    </style>
</head>

<body>
    <div class="container-login">
        <div class="login">
            <div class="right">
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQp4u86hW4ER9a9TGvp2yFcUW8SnYiY-n4pjhW5tXzTDfzU6TZ_ojCxHXlDz-Q-PnFi6PM&usqp=CAU"
                    alt="">
            </div>
            <h1>LOG-IN</h1>
            <form action="" method="POST" id="loginForm">
                <div class="alert alert-danger">
                </div>
                <input type="email" value="email" name="email" placeholder="example@gmail.com">
                <input type="password" name="password" placeholder="Password">
                <p>
                    <a href="#">Forgot Password?</a>
                </p>
                <button name="submit" type="submit" id="loginButton">
                    <span id="buttonText">Log-In</span>
                    <div class="loading" id="loadingDiv"></div>
                </button>
            </form>
        </div>

        <script>
            document.getElementById('loginForm').addEventListener('submit', function (event) {
                event.preventDefault(); // Prevent the form from submitting normally

                // Show loading animation
                document.getElementById('loadingDiv').style.display = 'inline-block';

                // Disable the button while processing
                document.getElementById('loginButton').disabled = true;

                // Change button text to "Logging In..."
                document.getElementById('buttonText').innerText = 'Logging In...';

                // Simulate login process (replace this with your actual login logic)
                setTimeout(function () {
                    // Once login is successful, you can redirect the user or perform other actions
                    // For now, we'll reset the button and hide the loading animation
                    document.getElementById('loginButton').disabled = false;
                    document.getElementById('buttonText').innerText = 'Log-In';
                    document.getElementById('loadingDiv').style.display = 'none';
                }, 2000); // Simulated 2-second login process
            });
        </script>
    </div>
</body>

</html>
