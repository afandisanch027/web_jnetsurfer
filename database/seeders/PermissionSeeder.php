<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role_admin = Role::updateOrCreate([
            'name' => 'admin',
        ],[
            'name' => 'admin',
        ]);

        $role_sales = Role::updateOrCreate([
            'name' => 'sales',
        ],[
            'name' => 'sales',
        ]);

        $role_teknisi = Role::updateOrCreate([
            'name' => 'teknisi',
        ],[
            'name' => 'teknisi',
        ]);

        $role_inventory = Role::updateOrCreate([
            'name' => 'inventory',
        ],[
            'name' => 'inventory',
        ]);

        $role_finance = Role::updateOrCreate([
            'name' => 'finance',
        ],[
            'name' => 'finance',
        ]);

        $permission = Permission::updateOrCreate(
            [
                'name' => 'view_dashboard_admin',
            ],
            [
                'name' => 'view_dashboard_admin',
            ]
        );
        $permission2 = Permission::updateOrCreate(
            [
                'name' => 'view_dashboard_inventory',
            ],
            [
                'name' => 'view_dashboard_inventory',
            ]
        );
        $permission3 = Permission::updateOrCreate(
            [
                'name' => 'view_dashboard_finance',
            ],
            [
                'name' => 'view_dashboard_finance',
            ]
        );

        $role_admin->givePermissionTo($permission);
        $role_inventory->givePermissionTo($permission2);
        $role_finance->givePermissionTo($permission3);

        $user = User::find(1);
        $user2 = User::find(2);
        $user3 = User::find(5);

        $user ->assignRole('admin');
        $user2 ->assignRole('inventory');
        $user3 ->assignRole('finance');


    }
}
