<?php

namespace Database\Seeders;

use App\Models\Sales;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = \Faker\Factory::create('id_ID');
        for($i = 0; $i <10; $i++){
            Sales::create([
                'plgName' => $faker->name,
                'noHp' => $faker->phoneNumber,
                'alamat'=> $faker->address,
                'paket' =>$faker->sentence,
                'harga' =>$faker->sentence,
                'email' =>$faker->sentence,
                'status' =>$faker->sentence,

            ]);
        }
    }
}
