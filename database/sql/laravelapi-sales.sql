-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2023 at 05:50 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelapi-sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('admin','sales','teknisi','inventory','finance') NOT NULL DEFAULT 'admin',
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(6, 'Administrator', 'admin@gmail.com', 'admin', '$2y$10$ONQ79XddBctyS1oJf0a5tOM4UHlEo/Rkj9jYaClOZpr5v5LmeuKRq', NULL, '2023-10-22 21:28:43', '2023-10-22 21:28:43'),
(7, 'Sales', 'sales@gmail.com', 'sales', '$2y$10$p3klgy/ExrNoL6MYTJqtAu6.BoUR3B9NIpDrMAWHR4KeS6dMDcuwy', NULL, '2023-10-22 21:28:43', '2023-10-22 21:28:43'),
(8, 'Teknisi', 'teknisi@gmail.com', 'teknisi', '$2y$10$2BABVdVzcIbO.5VTyWIGiO0p/kcw0Uy.4YeS2w5zi.ZlHbLXkfg22', NULL, '2023-10-22 21:28:43', '2023-10-22 21:28:43'),
(9, 'Inventory', 'inventory@gmail.com', 'inventory', '$2y$10$PgW5TMgqBXl2LvflRVzDFud1Pos/efyyooXWay10B1zTdh1uYfl72', NULL, '2023-10-22 21:28:44', '2023-10-22 21:28:44'),
(10, 'Finance', 'finance@gmail.com', 'finance', '$2y$10$ixJMO9C7yAdoXKFRTWP3s.wJNqKivrMyj4SvAc0yUSwSAVLOyAhg2', NULL, '2023-10-22 21:28:44', '2023-10-22 21:28:44');

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clears`
--

CREATE TABLE `clears` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_10_14_125144_create_sales_table', 1),
(6, '2023_10_17_032401_create_clears_table', 2),
(7, '2023_10_17_032445_create_logins_table', 2),
(8, '2023_10_23_033713_create_admins_table', 2),
(9, '2023_10_24_084442_create_permission_tables', 3),
(10, '2023_10_26_031650_create_barangs_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(1, 'App\\Models\\User', 7),
(3, 'App\\Models\\User', 9),
(4, 'App\\Models\\User', 2),
(5, 'App\\Models\\User', 5),
(5, 'App\\Models\\User', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view_dashboard', 'web', '2023-10-24 02:10:37', '2023-10-24 02:10:37'),
(2, 'view_chart_on_dashboard', 'web', '2023-10-24 04:36:29', '2023-10-24 04:36:29'),
(3, 'view_dashboard_admin', 'web', '2023-10-24 05:59:06', '2023-10-24 05:59:06'),
(4, 'view_dashboard_inventory', 'web', '2023-10-24 05:59:06', '2023-10-24 05:59:06'),
(5, 'view_dashboard_finance', 'web', '2023-10-24 05:59:06', '2023-10-24 05:59:06');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2023-10-24 01:58:08', '2023-10-24 01:58:08'),
(2, 'sales', 'web', '2023-10-24 01:58:08', '2023-10-24 01:58:08'),
(3, 'teknisi', 'web', '2023-10-24 01:58:08', '2023-10-24 01:58:08'),
(4, 'inventory', 'web', '2023-10-24 01:58:08', '2023-10-24 01:58:08'),
(5, 'finance', 'web', '2023-10-24 01:58:08', '2023-10-24 01:58:08');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(2, 4),
(2, 5),
(3, 1),
(4, 4),
(5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plgName` varchar(255) NOT NULL,
  `noHp` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `paket` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `status` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `lat` decimal(9,6) NOT NULL,
  `lon` decimal(9,6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `plgName`, `noHp`, `alamat`, `paket`, `harga`, `status`, `email`, `lat`, `lon`, `created_at`, `updated_at`) VALUES
(1, 'Jane Titin Lailasari', '0503 0156 608', 'Jln. Badak No. 637, Subulussalam 22147, Papua', '30Mbps', 'Rp125.00/bln', 'waiting', 'tetsing1@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(2, 'Karen Nilam Namaga', '0452 5192 126', 'Jr. Villa No. 488, Metro 98488, Papua', '10Mbps', 'Rp75.000/bln', 'waiting', 'test@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(3, 'Febi Wahyuni S.IP', '0617 6284 6838', 'Psr. Zamrud No. 58, Tangerang 45380, Gorontalo', '30Mbps', 'Rp125.000/bln', 'waiting', 'jhdwhsd@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(12, 'delia', '0314 8976 7345', 'Ds. Alkmar No.374 Pasuruan 234 Jawa Timur', '10Mbps', 'Rp. 75.000/bln', 'waiting', 'tgjskk@gmail.com', 0.000000, 0.000000, '2023-10-15 06:28:49', '2023-10-15 06:28:49'),
(13, 'asddasd', 'asdasdasd', 'asdasdasdasd', '50Mbps', 'Rp150.000/bln', 'Waiting', 'asdasdda', 0.000000, 0.000000, '2023-10-20 02:08:24', '2023-10-20 02:08:24'),
(14, 'fsdfsdf', 'sdfsdfsf', 'sdfsdfs', '10Mbps', 'Rp75.000/bln', 'Waiting', 'sdfdfsf', 0.000000, 0.000000, '2023-10-20 02:16:29', '2023-10-20 02:16:29'),
(15, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:18:57', '2023-10-20 02:18:57'),
(16, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:20:16', '2023-10-20 02:20:16'),
(17, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:20:36', '2023-10-20 02:20:36'),
(18, 'dsasdw', 'sadadasd', 'asdasda', '10Mbps', 'Rp75.000/bln', 'Waiting', 'asdasdas', 0.000000, 0.000000, '2023-10-22 23:49:03', '2023-10-22 23:49:03'),
(19, 'asdassd', 'asdsad', 'asdasd', '30Mbps', 'Rp125.000/bln', 'Waiting', 'asdsadsad', 0.000000, 0.000000, '2023-10-23 02:08:03', '2023-10-23 02:08:03'),
(20, 'adasdasdsad', 'asdasd', 'ewregerg', '30Mbps', 'Rp125.000/bln', 'Waiting', 'gergegeg', 37.422675, -122.084994, '2023-10-25 03:48:24', '2023-10-25 03:48:24'),
(21, 'xixi', 'xixi', 'xixi', '30Mbps', 'Rp125.000/bln', 'Waiting', 'xixi', 37.425587, -122.083823, '2023-10-25 20:42:02', '2023-10-25 20:42:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nik` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nik`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1234, 'Nur Hidayah Afandi', 'afandi@gmail.com', NULL, '$2y$10$uYhSi4fawwkuCDr1XRuEdOBk4gQf7TvheuLJ6/DzLw3jSrDK7dPwm', NULL, '2023-10-24 00:33:41', '2023-10-24 00:33:41'),
(2, 1235, 'sanches', 'sanches@gmail.com', NULL, '$2y$10$uYhSi4fawwkuCDr1XRuEdOBk4gQf7TvheuLJ6/DzLw3jSrDK7dPwm', NULL, '2023-10-24 00:33:41', '2023-10-24 00:33:41'),
(5, 1236, 'nur', 'nur@gmail.com', NULL, '$2y$10$uYhSi4fawwkuCDr1XRuEdOBk4gQf7TvheuLJ6/DzLw3jSrDK7dPwm', NULL, '2023-10-24 00:33:41', '2023-10-24 00:33:41'),
(6, 12345, 'ujhjuk', 'aku@gmail.com', NULL, '$2y$10$1B240KhERLATu55svVdQA.cMSEFf0YTfscDSQZHsaSBEtlEviaTXq', NULL, '2023-10-24 21:10:18', '2023-10-24 21:10:18'),
(7, 12345, 'ujhjuk', 'aku1@gmail.com', NULL, '$2y$10$y98rxEjg/aglTIjtUqM8xeOjLuzpJTkoylNrjO54uXij/4W7ek5aS', NULL, '2023-10-24 21:33:37', '2023-10-24 21:33:37'),
(8, 19, 'Beverly Lamb', 'hekujuja@mailinator.com', NULL, '$2y$10$7GSuCj5pO43YawfRxTXFa.Sv4o4YrIVSm.C2oOVMsU60jhNSmpc1G', NULL, '2023-10-24 21:37:43', '2023-10-24 21:37:43'),
(9, 3567, 'jhjkh', 'testing@gmail.com', NULL, '$2y$10$pNE1bab3bP4OYghC6ssQ1u/usKSULOOck5r9rAbjvpozoIyeRFm1a', NULL, '2023-10-24 21:46:38', '2023-10-24 21:46:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clears`
--
ALTER TABLE `clears`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clears`
--
ALTER TABLE `clears`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
