-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2023 at 06:50 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelapi-sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `plgName` varchar(255) NOT NULL,
  `noHp` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `paket` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `status` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `lat` decimal(9,6) NOT NULL,
  `lon` decimal(9,6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `plgName`, `noHp`, `alamat`, `paket`, `harga`, `status`, `email`, `lat`, `lon`, `created_at`, `updated_at`) VALUES
(1, 'Jane Titin Lailasari', '0503 0156 608', 'Jln. Badak No. 637, Subulussalam 22147, Papua', '30Mbps', 'Rp125.00/bln', 'waiting', 'tetsing1@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(2, 'Karen Nilam Namaga', '0452 5192 126', 'Jr. Villa No. 488, Metro 98488, Papua', '10Mbps', 'Rp75.000/bln', 'waiting', 'test@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(3, 'Febi Wahyuni S.IP', '0617 6284 6838', 'Psr. Zamrud No. 58, Tangerang 45380, Gorontalo', '30Mbps', 'Rp125.000/bln', 'waiting', 'jhdwhsd@gmail.com', 0.000000, 0.000000, '2023-10-14 06:40:25', '2023-10-14 06:40:25'),
(12, 'delia', '0314 8976 7345', 'Ds. Alkmar No.374 Pasuruan 234 Jawa Timur', '10Mbps', 'Rp. 75.000/bln', 'waiting', 'tgjskk@gmail.com', 0.000000, 0.000000, '2023-10-15 06:28:49', '2023-10-15 06:28:49'),
(13, 'asddasd', 'asdasdasd', 'asdasdasdasd', '50Mbps', 'Rp150.000/bln', 'Waiting', 'asdasdda', 0.000000, 0.000000, '2023-10-20 02:08:24', '2023-10-20 02:08:24'),
(14, 'fsdfsdf', 'sdfsdfsf', 'sdfsdfs', '10Mbps', 'Rp75.000/bln', 'Waiting', 'sdfdfsf', 0.000000, 0.000000, '2023-10-20 02:16:29', '2023-10-20 02:16:29'),
(15, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:18:57', '2023-10-20 02:18:57'),
(16, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:20:16', '2023-10-20 02:20:16'),
(17, 'ibab', '1323123', 'USA', '50Mbps', 'Rp150.000/bln', 'Waiting', 'adsdasds', 0.000000, 0.000000, '2023-10-20 02:20:36', '2023-10-20 02:20:36'),
(18, 'dsasdw', 'sadadasd', 'asdasda', '10Mbps', 'Rp75.000/bln', 'Waiting', 'asdasdas', 0.000000, 0.000000, '2023-10-22 23:49:03', '2023-10-22 23:49:03'),
(19, 'asdassd', 'asdsad', 'asdasd', '30Mbps', 'Rp125.000/bln', 'Waiting', 'asdsadsad', 0.000000, 0.000000, '2023-10-23 02:08:03', '2023-10-23 02:08:03'),
(20, 'adasdasdsad', 'asdasd', 'ewregerg', '30Mbps', 'Rp125.000/bln', 'Waiting', 'gergegeg', 37.422675, -122.084994, '2023-10-25 03:48:24', '2023-10-25 03:48:24'),
(21, 'xixi', 'xixi', 'xixi', '30Mbps', 'Rp125.000/bln', 'Waiting', 'xixi', 37.425587, -122.083823, '2023-10-25 20:42:02', '2023-10-25 20:42:02'),
(22, 'adasdasd', 'asdasd', 'asdsa', '30Mbps', 'Rp125.000/bln', 'Waiting', 'asdasd', 37.422675, -122.084994, '2023-10-25 20:55:44', '2023-10-25 20:55:44'),
(23, 'adasdasd', 'asdasd', 'asdsa', '30Mbps', 'Rp125.000/bln', 'Waiting', 'asdasd', 37.422675, -122.084994, '2023-10-25 20:56:08', '2023-10-25 20:56:08'),
(24, '1111', '1111', '1111', '10Mbps', 'Rp75.000/bln', 'Waiting', '111', 37.422675, -122.084994, '2023-10-25 21:00:47', '2023-10-25 21:00:47'),
(25, 'xxxx', 'xxxx', 'xxxxx', '10Mbps', 'Rp75.000/bln', 'Waiting', 'xxx', 37.422675, -122.084994, '2023-10-25 21:08:39', '2023-10-25 21:08:39'),
(26, '4444', '444', '444', '30Mbps', 'Rp125.000/bln', 'Waiting', '444', 37.422675, -122.084994, '2023-10-25 21:09:59', '2023-10-25 21:09:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
