<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
{
    Schema::create('jadwal_psb', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger('sales_id');
        $table->date('tgl_psb');
        $table->string('status');
        $table->string('team');
        $table->timestamps();
    });
}

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('jadwal_psb');
    }
};
