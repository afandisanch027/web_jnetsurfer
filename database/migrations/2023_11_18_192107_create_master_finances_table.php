<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('master_finance', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal');
            $table->string('keperluan');
            $table->text('catatan')->nullable();
            $table->decimal('nominal_in', 10, 2);
            $table->decimal('nominal_out', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('master_finance');
    }
};
