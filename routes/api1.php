<?php
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\PayingController;
use App\Http\Controllers\Api\SalesController;
use App\Http\Controllers\Api\TeknisiController;
use App\Http\Controllers\Api\ScheduleController;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// Route::post('register', [AuthController::class,'Register'] );
Route::post('login', [AuthController::class,'Login'] );

Route::get('/users', [UserController::class, 'getAllUsers'])->middleware('role:finance');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Sales
Route::get('sales',[SalesController::class,'index']);
Route::get('sales/{id}',[SalesController::class,'show']);
Route::post('sales',[SalesController::class,'store']);
Route::put('sales/{id}',[SalesController::class,'update']);
Route::delete('sales/{id}',[SalesController::class,'destroy']);

//teknisi
Route::get('teknisi',[TeknisiController::class,'index']);
Route::get('teknisi/{status}', [TeknisiController::class, 'showByStatus']);
// Route::get('teknisi/{id}',[TeknisiController::class,'show']);
Route::post('teknisi',[TeknisiController::class,'store']);
Route::post('teknisi/{id}',[TeknisiController::class,'update']);
Route::delete('teknisi/{id}',[TeknisiController::class,'destroy']);

//paying
Route::post('paying',[PayingController::class,'store']);
Route::get('paying',[PayingController::class,'index']);

//schedule
Route::get('schedule',[ScheduleController::class,'index']);
Route::post('schedule',[ScheduleController::class,'store']);
Route::post('schedule/{id}',[ScheduleController::class,'update']);
