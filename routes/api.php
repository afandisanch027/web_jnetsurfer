<?php

use App\Http\Controllers\Api\SalesController;
use App\Http\Controllers\Api\TeknisiController;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BarangController;
use App\Http\Controllers\Api\PakeLayananController;
use App\Http\Controllers\Api\PayingController;
use App\Http\Controllers\Api\ScheduleController;
use App\Models\Teknisi;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
// Route::get('login', [AuthController::class,'Login'] );

// Route::get('/users', [UserController::class, 'getAllUsers'])->middleware('role:finance');

// Route::resource('barang', 'BarangController');


// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('login', [AuthController::class, 'login']);
// Route::post('logout', [AuthController::class, 'logout'])->middleware('auth.api');
Route::post('/login', [AuthController::class, 'login']);
Route::middleware('auth:sanctum')->post('/logout', [AuthController::class, 'logout']);

// Route::group(["middleware" => ["auth:sanctum", 'authApi']], function () {
//     Route::post("/logout", [AuthController::class, "logout"]);
// });

// API_SALES_UTAMA
Route::get('sales',[SalesController::class,'index']);
// Route::get('sales/{status}', [SalesController::class, 'showByStatus']);
Route::post('/sales',[SalesController::class,'store']);
Route::get('/sales/{id}', [SalesController::class, 'showById']);
Route::get('/sales/status/{status}', [SalesController::class, 'showByStatus']);

// Route::get('sales/{id}',[SalesController::class,'show']);
// Route::put('sales/{id}',[SalesController::class,'update']);
// Route::delete('sales/{id}',[SalesController::class,'destroy']);

// API_TEKNISI_UTAMA
Route::get('teknisi',[TeknisiController::class,'index']);
Route::get('teknisi/{status_teknisi}', [TeknisiController::class, 'showByStatus']);
Route::post('teknisi/{id}',[TeknisiController::class,'update']);

// Route::post('teknisi',[TeknisiController::class,'store']);
// Route::get('teknisi/{id}',[TeknisiController::class,'show']);
// Route::post('teknisi',[TeknisiController::class,'store']);
// Route::post('teknisi/{id}',[TeknisiController::class,'update']);
// Route::delete('teknisi/{id}',[TeknisiControllerr::class,'destroy']);

Route::get('/teknisi_survei', [TeknisiController::class, 'getSelectedData']);


Route::post('/barang', [BarangController::class, 'store']);
Route::get('/barang', [BarangContsroller::class, 'index']);

Route::get('/paying', [PayingController::class, 'index']);
Route::post('/paying', [PayingController::class, 'store']);

//schedule
Route::get('schedule',[ScheduleController::class,'index']);
Route::post('schedule',[ScheduleController::class,'store']);
Route::post('schedule/{id}',[ScheduleController::class,'update']);
Route::get('schedule/{status}', [ScheduleController::class, 'showByStatus']);


//Paket_Layanan
Route::get('paketlayanan',[PakeLayananController::class,'index']);
