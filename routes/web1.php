<?php

use App\Http\Controllers\GudangCekController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JadwalSurveiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\WebSalesController;
use App\Http\Controllers\WebTeknisiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// testing
// Route::view('/', 'home');
// Route::view('/about', 'about');
// Route::view('/settings', 'settings');

// Route::get('/', [HomeController::class, 'index'])->name('home');

//  bawah asli
Route::get('/',[LoginController::class,'index'])->name('login');
Route::post('/login-proses',[LoginController::class,'login_proses'])->name('login-proses');
Route::get('/logout',[LoginController::class,'logout'])->name('logout');

Route::get('/register',[LoginController::class,'register'])->name('register');
Route::post('/register-proses',[LoginController::class,'register_proses'])->name('register-proses');



Route::group(['prefix' => 'admin','middleware' => ['auth'], 'as' => 'admin.' ] , function(){
    Route::get('/',[HomeController::class,'dashboard'])->name('dashboard');
});



// Route::get('/', function () {
//     return view('dashboard_admin');
// });

// Route::get('/survei', function () {
//     return view('sales/survei');
// });
Route::get('/jadwal_survei', [WebSalesController::class, 'index'])->name('sales.index');

Route::get('/hasil_survei', [WebTeknisiController::class, 'index'])->name('teknisi.index');
Route::get('/hasil_psb', [WebTeknisiController::class, 'psb'])->name('teknisi.psb');
Route::get('/gudang_cek', [GudangCekController::class, 'index'])->name('inventory.index');
// Route::get('/teknisi/{sales_id}', [TeknisiController::class, 'show']);


// Route::get('/hasil_survei', function () {
//     return view('teknisi/hasil_survei');
// });

Route::get('/form_survei', function () {
    return view('sales/form_survei');
});

Route::get('/pemasangan_baru', function () {
    return view('teknisi/pemasangan_baru');
});

Route::get('/aktivasi_psb', function () {
    return view('teknisi/aktivasi_psb');
});

Route::get('/permintaan_baru', function () {
    return view('inventory/permintaan_baru');
});

Route::get('/form_permintaan_baru', function () {
    return view('inventory/form_permintaan_baru');
});

Route::get('/stock_barang', function () {
    return view('inventory/stock_barang');
});

Route::get('/stock_barang_masuk', function () {
    return view('inventory/stock_barang_masuk');
});

Route::get('/create_barang_masuk', function () {
    return view('inventory/create_barang_masuk');
});
Route::get('/edit_barang_masuk', function () {
    return view('inventory/edit_barang_masuk');
});

Route::get('/stock_barang_keluar', function () {
    return view('inventory/stock_barang_keluar');
});

Route::get('/create_barang_keluar', function () {
    return view('inventory/create_barang_keluar');
});

Route::get('/edit_barang_keluar', function () {
    return view('inventory/edit_barang_keluar');
});

Route::get('/admin_gudang', function () {
    return view('admin/admin_gudang');
});

Route::get('/account', function () {
    return view('admin/account');
});

Route::get('/admin_finance', function () {
    return view('admin/admin_finance');
});

Route::get('/data_pemasukan', function () {
    return view('finance/data_pemasukan');
});

Route::get('/create_data_pemasukan', function () {
    return view('finance/create_data_pemasukan');
});

Route::get('/edit_data_pemasukan', function () {
    return view('finance/edit_data_pemasukan');
});

Route::get('/data_pengeluaran', function () {
    return view('finance/data_pengeluaran');
});

Route::get('/create_data_pengeluaran', function () {
    return view('finance/create_data_pengeluaran');
});

Route::get('/edit_data_pengeluaran', function () {
    return view('finance/edit_data_pengeluaran');
});

Route::get('/laporan_keuangan', function () {
    return view('finance/laporan_keuangan');
});

Route::get('/komisi', function () {
    return view('finance/komisi');
});


// Route::get('/survei',[WebSalesController::class, 'getAllSales']);
Route::get('/form_jadwal_survei/{sales_id}',[WebSalesController::class, 'tampilkanForm']);
Route::get('/form_gudang_cek/{sales_id}',[GudangCekController::class, 'tampilkanFormGudangCek']);

// Route::get('/waiting/{sales_id}', 'WebSalesController@tampilkanForm');



// Route::get('/waiting', function () {
//     return view('sales/waiting');
// });

// Route::get('/proses', function () {
//     return view('sales/proses');
// });
// Route::get('/selesai', function () {
//     return view('sales/selesai');
// });
// Route::get('/sales', 'WebSalesController@getAllSales');
// Route::get('/sales','WebSalesController@getSalesById');
// Route::get('/sales/status/{status','WebSalesController@getSalesByStatus');

Route::post('/submit/jadwal_survei', [JadwalSurveiController::class, 'submitJadwalSurvei'])->name('submit.jadwal_survei');
Route::post('/gudang_cek', [GudangCekController::class, 'submitGudangCek'])->name('submit.gudang_cek');
// Route::get('/survei1', 'JadwalSurveiController@survei')->name('survei');
