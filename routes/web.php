<?php

use App\Http\Controllers\GudangCekController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\JadwalPsbController;
use App\Http\Controllers\JadwalSurveiController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\MasterFinanceController;
use App\Http\Controllers\PaketLayananController;
use App\Http\Controllers\WebSalesController;
use App\Http\Controllers\WebTeknisiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[LoginController::class,'index'])->name('login');
Route::post('/login-proses',[LoginController::class,'login_proses'])->name('login-proses');
Route::get('/logout',[LoginController::class,'logout'])->name('logout');

Route::get('/register',[LoginController::class,'register'])->name('register');
Route::post('/register-proses',[LoginController::class,'register_proses'])->name('register-proses');



Route::group(['prefix' => 'admin','middleware' => ['auth'], 'as' => 'admin.' ] , function(){
    Route::get('/',[HomeController::class,'dashboard'])->name('dashboard');
});

//Sales
Route::get('/jadwal_survei', [WebSalesController::class, 'index'])->name('sales.index');
Route::get('/form_jadwal_survei/{sales_id}',[WebSalesController::class, 'tampilkanForm']);


//Teknisi
Route::get('/hasil_survei', [WebTeknisiController::class, 'index'])->name('teknisi.index');
Route::get('/form_jadwal_psb/{sales_id}', [JadwalPsbController::class, 'showJadwalPsbForm']);
Route::get('/hasil_psb', [WebTeknisiController::class, 'psb'])->name('teknisi.psb');
Route::get('/form_aktivasi_psb/{sales_id}', [JadwalPsbController::class, 'showAktivasiPsbForm']);
Route::post('/submit/jadwal_survei', [JadwalSurveiController::class, 'submitJadwalSurvei'])->name('submit.jadwal_survei');
Route::post('/submit_jadwal_psb', [JadwalPsbController::class, 'submitJadwalPsb'])->name('submit.jadwal_psb');
Route::post('/submit/aktivasi_psb', [JadwalPsbController::class, 'aktivasiPsb'])->name('submit.aktivasi_psb');

//Gudang
Route::get('/gudang_cek', [GudangCekController::class, 'index'])->name('inventory.index');
Route::get('/form_gudang_cek/{sales_id}',[GudangCekController::class, 'tampilkanFormGudangCek']);
Route::post('/gudang_cek', [GudangCekController::class, 'submitGudangCek'])->name('submit.gudang_cek');

//Data Master Barang
Route::get('/data_barang', [MasterBarangController::class, 'index'])->name('inventory.data_barang');
Route::get('/form_data_barang', [MasterBarangController::class, 'createFormDataBarang']);
Route::post('/submit_data_barang', [MasterBarangController::class, 'submitDataBarang'])->name('submit.data_barang');
Route::get('/edit_data_barang/{id}', [MasterBarangController::class, 'editDataBarang']);
Route::post('/submit_edit_data_barang', [MasterBarangController::class, 'submitEditDataBarang']);
Route::get('/barang_masuk', [MasterBarangController::class, 'barang_masuk'])->name('inventory.barang_masuk');
Route::get('/form_barang_masuk', [MasterBarangController::class, 'createFormBarangMasuk']);
Route::post('/submit_barang_masuk', [MasterBarangController::class, 'submitBarangMasuk'])->name('submit.barang_masuk');
Route::get('/edit_barang_masuk/{id}', [MasterBarangController::class, 'editBarangMasuk'])->name('inventory.barang.edit_barang_masuk');
Route::post('/submit_edit_barang_masuk/{id}', [MasterBarangController::class, 'submitEditBarangMasuk'])->name('submit_edit_barang_masuk');
Route::get('/barang_keluar', [MasterBarangController::class, 'barang_keluar'])->name('inventory.barang_keluar');
Route::get('/form_barang_keluar', [MasterBarangController::class, 'createFormBarangKeluar']);
Route::post('/submit_barang_keluar', [MasterBarangController::class, 'submitBarangKeluar'])->name('submit.barang_keluar');
Route::get('/edit_barang_keluar/{id}', [MasterBarangController::class, 'editBarangKeluar'])->name('inventory.barang.edit_barang_keluar');
Route::post('/submit_edit_barang_keluar/{id}', [MasterBarangController::class, 'submitEditBarangKeluar'])->name('submit_edit_barang_keluar');
Route::get('/stock_barang', [MasterBarangController::class, 'stockBarang'])->name('inventory.stock_barang');


//paket_layanan
Route::get('/paket_layanan', [PaketLayananController::class, 'index'])->name('paketlayanan.index');
Route::get('/form_paket_layanan', [PaketLayananController::class, 'createFormPaketLayanan']);
Route::post('/submit_paket_layanan', [PaketLayananController::class, 'submitPaketLayanan'])->name('submit.paket_layanan');


//Finance
Route::get('/data_masuk', [MasterFinanceController::class, 'data_masuk'])->name('finance.data_pemasukan');
Route::get('/data_keluar', [MasterFinanceController::class, 'data_keluar'])->name('finance.data_pengeluaran');
Route::get('/data_laporan', [MasterFinanceController::class, 'data_laporan'])->name('finance.data_laporan');
Route::get('/data_komisi', [MasterFinanceController::class, 'data_komisi'])->name('finance.data_komisi');
Route::get('/form_data_masuk', [MasterFinanceController::class, 'createFormDataMasuk']);
Route::get('/form_data_keluar', [MasterFinanceController::class, 'createFormDataKeluar']);
Route::post('/submit_data_masuk', [MasterFinanceController::class, 'submitDataMasuk'])->name('submit.data_masuk');
Route::post('/submit_data_keluar', [MasterFinanceController::class, 'submitDataKeluar'])->name('submit.data_keluar');
Route::get('/edit_data_masuk/{id}', [MasterFinanceController::class, 'editDataMasuk']);
Route::get('/edit_data_keluar/{id}', [MasterFinanceController::class, 'editDataKeluar']);
Route::post('/submit_edit_data_masuk', [MasterFinanceController::class, 'submitEditDataMasuk']);
Route::post('/submit_edit_data_keluar', [MasterFinanceController::class, 'submitEditDataKeluar']);







