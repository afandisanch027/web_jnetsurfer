<?php

namespace App\Models;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class Barang extends Model
{
    protected $table = "barangs";
    protected $fillable = ['sales_id', 'barang_id', 'nama_barang', 'quantity', 'qty_out'];

    public function masterBarang()
    {
        return $this->belongsTo(MasterBarang::class, 'barang_id');
    }

    public function sales()
    {
        return $this->belongsTo(Sales::class);
    }

    public function masukBarangs()
    {
        return $this->hasMany(MasukBarang::class, 'barang_id');
    }

    public function keluarBarangs()
    {
        return $this->hasMany(KeluarBarang::class, 'barang_id');
    }
}
