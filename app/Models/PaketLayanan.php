<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaketLayanan extends Model
{

    protected $table = 'paket_layanan';
    protected $fillable = ['kecepatan', 'satuan', 'harga', 'type'];

    use HasFactory;
}
