<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeluarBarang extends Model
{
    protected $table = 'keluar_barang';

    protected $fillable = [
        'id_barang',
        'tanggal',
        'quantity',
    ];

    public function masterBarang()
    {
        return $this->belongsTo(MasterBarang::class, 'id_barang');
    }

    use HasFactory;
}
