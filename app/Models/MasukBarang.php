<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasukBarang extends Model
{
    protected $table = 'masuk_barang';

    protected $fillable = [
        'id_barang',
        'tanggal',
        'quantity',
    ];

    public function masterBarang()
    {
        return $this->belongsTo(MasterBarang::class, 'id_barang');
    }
    public function barang()
{
    return $this->belongsTo(Barang::class, 'id_barang');
}

    use HasFactory;
}
