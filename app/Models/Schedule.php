<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = "jadwal_psb";
    protected $fillable = ['sales_id','tgl_psb','status','team'];
    use HasFactory;
}
