<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paying extends Model
{
    protected $table = "paying";
    protected $fillable = ['sales_id','metode','bukti_pembayaran'];
    use HasFactory;

    
}
