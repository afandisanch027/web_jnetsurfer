<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JadwalPsb extends Model
{
    protected $table = "jadwal_psb";
    protected $fillable = ['sales_id', 'tgl_psb', 'team', 'status', 'status_teknisi', 'tgl_selesai_psb'];
    use HasFactory;
}
