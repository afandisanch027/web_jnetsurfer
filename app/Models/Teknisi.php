<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teknisi extends Model
{
    protected $table = "teknisi";
    protected $fillable = ['sales_id', 'tgl_survei', 'team', 'status'];
    use HasFactory;


}
