<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterFinance extends Model
{
    use HasFactory;

    protected $table = 'master_finance'; // Jika nama tabel tidak sesuai dengan konvensi Laravel

    protected $fillable = [
        'tanggal',
        'keperluan',
        'catatan',
        'nominal_in',
        'nominal_out',
    ];

}
