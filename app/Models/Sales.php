<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Sales extends Model
{
    use HasFactory;

    protected $table = "sales";
    protected $fillable = [
        'plgName', 'noHp', 'jalan', 'rt', 'rw', 'kelurahan_desa', 'kecamatan', 'kabupaten', 'paket', 'harga', 'status', 'email', 'lat', 'lon', 'noTicket'
    ];

    public function barangs()
    {
        return $this->hasMany(Barang::class, 'sales_id', 'id');
    }
    public function jadwalPsb()
    {
        return $this->hasOne(JadwalPsb::class);
    }
}
