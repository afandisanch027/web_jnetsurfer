<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GudangCek extends Model
{

    protected $table = "gudang_cek";
    protected $fillable = ['sales_id', 'status'];
    use HasFactory;

    public function barangs()
    {
        return $this->hasMany(Barang::class, 'sales_id');
    }
}
