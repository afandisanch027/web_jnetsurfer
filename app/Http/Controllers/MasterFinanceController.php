<?php

namespace App\Http\Controllers;

use App\Models\MasterFinance;
use Illuminate\Http\Request;

class MasterFinanceController extends Controller
{
    public function data_masuk()
    {
        $DataMasuk = MasterFinance::paginate(10);
        return view('finance.data_pemasukan', compact('DataMasuk'));
    }

    public function data_keluar()
    {
        $DataKeluar = MasterFinance::paginate(10);
        return view('finance.data_pengeluaran', compact('DataKeluar'));
    }

    public function data_laporan()
    {
        $DataLaporan = MasterFinance::paginate(10);
        return view('finance.laporan_keuangan', compact('DataLaporan'));
    }

    public function data_komisi()
    {
        $DataKomisi = MasterFinance::paginate(10);
        return view('finance.komisi', compact('DataKomisi'));
    }

    public function createFormDataMasuk()
    {
        return view('finance.form_data_masuk');
    }

    public function createFormDataKeluar()
    {
        return view('finance.form_data_keluar');
    }

    public function submitDataMasuk(Request $request)
    {
        $validatedData = $request->validate([
            'tanggal' => 'required',
            'keperluan' => 'required',
            'catatan' => 'required',
            'nominal_in' => 'required',
        ]);
        MasterFinance::create($validatedData);
        return redirect()->route('finance.data_pemasukan')->with('success', 'Data berhasil dibuat!');
    }

    public function submitDataKeluar(Request $request)
    {
        $validatedData = $request->validate([
            'tanggal' => 'required',
            'keperluan' => 'required',
            'catatan' => 'required',
            'nominal_out' => 'required',
        ]);
        MasterFinance::create($validatedData);
        return redirect()->route('finance.data_pengeluaran')->with('success', 'Data berhasil dibuat!');
    }

    public function editDataMasuk($id)
    {
        $datamasuk = MasterFinance::find($id);
        if (!$datamasuk) {
            return redirect()->route('data_masuk')->with('error', 'Data tidak ditemukan');
        }
        return view('finance.edit_data_pemasukan', compact('datamasuk'));
    }

    public function editDataKeluar($id)
    {
        $datakeluar = MasterFinance::find($id);
        if (!$datakeluar) {
            return redirect()->route('data_keluar')->with('error', 'Data tidak ditemukan');
        }
        return view('finance.edit_data_pengeluaran', compact('datakeluar'));
    }

    public function submitEditDataMasuk(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'keperluan' => 'required',
            'catatan' => 'required',
            'nominal_in' => 'required',
            
        ]);
        $data = [
            'tanggal' => $request->input('tanggal'),
            'keperluan' => $request->input('keperluan'),
            'catatan' => $request->input('catatan'),
            'nominal_in' => $request->input('nominal_in'),
        ];
        MasterFinance::updateOrInsert(
            ['id' => $request->input('id')],
            $data
        );
        return redirect()->route('finance.data_pemasukan')->with('success', 'Data berhasil diperbarui');
    }

    public function submitEditDataKeluar(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'keperluan' => 'required',
            'catatan' => 'required',
            'nominal_out' => 'required',
            
        ]);
        $data = [
            'tanggal' => $request->input('tanggal'),
            'keperluan' => $request->input('keperluan'),
            'catatan' => $request->input('catatan'),
            'nominal_out' => $request->input('nominal_out'),
        ];
        MasterFinance::updateOrInsert(
            ['id' => $request->input('id')],
            $data
        );
        return redirect()->route('finance.data_pengeluaran')->with('success', 'Data berhasil diperbarui');
    }

    
}
