<?php

namespace App\Http\Controllers;

use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;

class WebSalesController extends Controller
{
    public function index()
    {
        // $sales = Sales::all();
        $pagination = 10;
        $sales = Sales::paginate($pagination);

        return view('sales.jadwal_survei', ['sales' => $sales]);
    }

    public function tampilkanForm($sales_id)
    {
        $sales = Sales::find($sales_id);
        if (!$sales) {
            abort(404);
        }

        return view('sales.form_jadwal_survei', compact('sales'));
    }





    // public function submitJadwalSurvei(Request $request)
    // {
    //     $request->validate([
    //         'sales_id' => 'required',
    //         'tanggal_survei' => 'required|date',
    //         'team' => 'required',
    //     ]);

    //     // Dapatkan data Sales berdasarkan ID
    //     $sales = Sales::findOrFail($request->sales_id);

    //     // Simpan data Teknisi ke dalam tabel Teknisi
    //     Teknisi::create([
    //         'sales_id' => $sales->id,
    //         'tgl_survei' => $request->tanggal_survei,
    //         'team' => $request->team,
    //         'status' => $request->status,

    //     ]);
    //     return redirect()->route('jadwal_survei');
    // }

    // public function getAllSales()
    // {
    //     $pagination = 10;
    //     $sales = Sales::paginate($pagination);
    //     return view('sales.jadwal_survei', compact('sales'));
    // }    


    // public function getSalesById($id)
    // {
    //     $sales = Sales::find($id);
    //     return view('sales.show', compact('sales'));
    // }

    // public function getSalesByStatus($status)
    // {
    //     $sales = Sales::where('status', $status)->get();
    //     return view('sales.index', compact('sales'));
    // }



}
