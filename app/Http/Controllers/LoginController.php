<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class LoginController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function login_proses(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $data = [
            'email' => $request ->email,
            'password' => $request ->password,
        ];

        if(Auth::attempt($data)){
            return redirect()->route('admin.dashboard');
        } else{
            return redirect()->route('login')->with('failed','Email atau Password Salah');
        }
    }

    public function authenticated(Request $request, $user)
    {
        // return redirect('/home');
        return redirect()->route('admin.dashboard'); // Ganti '/home' dengan rute tujuan Anda setelah login berhasil.
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('login')->with('success','Kamu Berhasil Logout');
    }

    public function register(){
        $roles = Role::all();
        return view('auth.register', compact('roles'));
    }

    public function register_proses(Request $request){
        $request->validate([
            'nik' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
            'roles' => 'required',
        ]);

        $data['nik'] = $request->nik;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = Hash::make($request->password);


        $user = User::create($data);
        $user ->assignRole($request->roles);
            // return $user;
        $login = [
            'email' => $request ->email,
            'password' => $request ->password,
        ];

        if(Auth::attempt($login)){
            return redirect()->route('admin.dashboard');
        } else{
            return redirect()->route('login')->with('failed','Email atau Password Salah');
        }


    }

}
