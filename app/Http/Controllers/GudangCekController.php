<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\GudangCek;
use App\Models\JadwalPsb;
use App\Models\KeluarBarang;
use App\Models\MasukBarang;
use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GudangCekController extends Controller
{
    public function index()
    {
        $salesData = Sales::paginate(10);
        $gudang_cekData = GudangCek::all();
        $jadwal_psbData = JadwalPsb::all();

        return view('inventory.gudang_cek', compact('salesData', 'gudang_cekData', 'jadwal_psbData'));
    }

    public function tampilkanFormGudangCek($sales_id)
    {
        $salesData = Sales::find($sales_id);
        $barangs = $salesData->barangs;
        $jadwalPsb = $salesData->jadwalPsb;

        if (!$salesData) {
            abort(404);
        }

        return view('inventory.form_gudang_cek', compact('salesData', 'barangs', 'jadwalPsb'));
    }

    public function submitGudangCek(Request $request)
    {
        $request->validate([
            // Tambahkan aturan validasi sesuai kebutuhan
        ]);

        $sales_id = $request->input('sales_id');
        $gudangCek = GudangCek::where('sales_id', $sales_id)->first();

        if (!$gudangCek) {
            abort(404);
        }

        $jadwalPsb = JadwalPsb::where('sales_id', $sales_id)->first();

        if ($jadwalPsb) {
            switch ($jadwalPsb->status) {
                case 'Waiting_Psb':
                    $gudangCek->status = 'Proses_Cek';
                    break;
                case 'Proses_Psb':
                case 'Selesai_Psb':
                    $gudangCek->status = 'Selesai_Cek';
                    $jadwalPsb->update([
                        'status' => 'Selesai_Psb',
                        'status_teknisi' => 'Waiting_Teknisi_Psb',
                    ]);

                    // Tambahkan data ke keluar_barang
                    $this->pindahKeKeluarBarang($sales_id);

                    break;
            }

            $gudangCek->save();
        }

        return redirect()->route('inventory.index');
    }

    private function pindahKeKeluarBarang($sales_id)
    {
        // Ambil data barang yang ingin dipindahkan dari tabel 'barangs'
        $barangs = Barang::where('sales_id', $sales_id)->get();

        // Iterasi melalui data barang dan simpan ke dalam tabel 'keluar_barang'
        foreach ($barangs as $barang) {
            KeluarBarang::create([
                'barang_id' => $barang->id_barang, // Sesuaikan dengan nama kolom di tabel 'keluar_barang'
                'quantity' => $barang->quantity,
                'tanggal' => now(),
            ]);
        }

        // Hapus data dari tabel 'barangs' setelah dipindahkan
        Barang::where('sales_id', $sales_id)->delete();
    }
}
