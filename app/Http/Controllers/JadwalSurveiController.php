<?php

namespace App\Http\Controllers;

use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Http\Request;

class JadwalSurveiController extends Controller
{
    public function submitJadwalSurvei(Request $request)
    {

        // Memeriksa apakah ID sudah ada dalam database
        $existingTeknisi = Teknisi::where('sales_id', $request->sales_id)->first();

        if ($existingTeknisi) {
            return redirect()->route('sales.index')->with('error', 'Jadwal survei untuk ID ini sudah ada.');
        }

        $request->validate([
            'sales_id' => 'required',
            'tanggal_survei' => 'required|date',
            'team' => 'required',
        ]);

        // Dapatkan data Sales berdasarkan ID
        $sales = Sales::findOrFail($request->sales_id);

        // Simpan data Teknisi ke dalam tabel Teknisi
        Teknisi::create([
            'sales_id' => $sales->id,
            'tgl_survei' => $request->tanggal_survei,
            'team' => $request->team,
            'status' => 'Waiting_Survei',
        ]);
        // Perbarui status Sales menjadi 'Selesai'
        $sales->update(['status' => 'Selesai']);

        return redirect()->route('sales.index')->with('success', 'Jadwal survei berhasil disimpan.');
    }
}
