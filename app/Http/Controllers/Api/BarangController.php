<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\Barangs;
use App\Models\GudangCek;
use App\Models\JadwalPsb;
use App\Models\MasterBarang;
use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class BarangController extends Controller
{

    public function index()
    {
        $barang = MasterBarang::all();

        return response()->json($barang);
    }




    // public function store(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'sales_id' => 'required|exists:sales,id',
    //         'barang.*.barang_id' => 'required|exists:master_barang,id',
    //         'barang.*.quantity' => 'required|numeric',
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json([
    //             'status' => false,
    //             'message' => 'Gagal memasukkan data',
    //             'errors' => $validator->errors()
    //         ], 422);
    //     }

    //     // Cek apakah sales_id sudah ada
    //     $existingBarang = Barang::where('sales_id', $request->sales_id)->first();

    //     if ($existingBarang) {
    //         return response()->json([
    //             'status' => false,
    //             'message' => 'Sales ID ini sudah memiliki barang',
    //         ], 400);
    //     }

    //     DB::beginTransaction();

    //     try {
    //         foreach ($request->barang as $barang) {
    //             $dataBarang = new Barang();
    //             $dataBarang->sales_id = $request->sales_id;
    //             $dataBarang->barang_id = $barang['barang_id'];
    //             $dataBarang->quantity = $barang['quantity'];
    //             $dataBarang->save();
    //         }

    //         // Update status Teknisi hanya jika tidak ada barang sebelumnya
    //         $teknisi = Teknisi::where('sales_id', $request->sales_id)->first();
    //         if ($teknisi && !$existingBarang) {
    //             $teknisi->status = 'Selesai_Survei';
    //             $teknisi->save();
    //         }

    //         // Tambahkan entri GudangCek hanya jika tidak ada barang sebelumnya
    //         $gudangCek = new GudangCek();
    //         $gudangCek->sales_id = $request->sales_id;
    //         $gudangCek->status = 'Waiting_Cek';
    //         $gudangCek->save();

    //         // Buat status 'waiting_psb' di tabel jadwal_psb
    //         $this->createWaitingPsb($request->sales_id);

    //         // Commit transaksi
    //         DB::commit();

    //         return response()->json([
    //             'status' => true,
    //             'message' => 'Berhasil memasukkan data',
    //         ], 200);
    //     } catch (\Exception $e) {
    //         // Tangkap kesalahan dan rollback transaksi jika terjadi kesalahan
    //         DB::rollBack();

    //         return response()->json([
    //             'status' => false,
    //             'message' => 'Terjadi kesalahan: ' . $e->getMessage(),
    //         ], 500);
    //     }
    // }

    // public function createWaitingPsb($sales_id)
    // {
    //     JadwalPsb::create([
    //         'sales_id' => $sales_id,
    //         'status' => 'Waiting_Psb',
    //     ]);
    // }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sales_id' => 'required|exists:sales,id',
            'barang.*.nama_barang' => 'required|exists:master_barang,nama_barang',
            'barang.*.quantity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memasukkan data',
                'errors' => $validator->errors()
            ], 422);
        }

        // Cek apakah sales_id sudah ada
        $existingBarang = Barang::where('sales_id', $request->sales_id)->first();

        if ($existingBarang) {
            return response()->json([
                'status' => false,
                'message' => 'Sales ID ini sudah memiliki barang',
            ], 400);
        }

        DB::beginTransaction();

        try {
            foreach ($request->barang as $barang) {
                // Dapatkan ID dan nama_barang dari master_barang berdasarkan nama_barang
                $masterBarang = MasterBarang::where('nama_barang', $barang['nama_barang'])->first();

                // Buat entry baru di Barang
                $dataBarang = new Barang();
                $dataBarang->sales_id = $request->sales_id;
                $dataBarang->barang_id = $masterBarang->id; // Menggunakan ID dari master_barang
                $dataBarang->nama_barang = $barang['nama_barang']; // Simpan nama_barang juga
                $dataBarang->quantity = $barang['quantity'];
                $dataBarang->save();
            }

            // Update status Teknisi hanya jika tidak ada barang sebelumnya
            $teknisi = Teknisi::where('sales_id', $request->sales_id)->first();
            if ($teknisi && !$existingBarang) {
                $teknisi->status = 'Selesai_Survei';
                $teknisi->save();
            }

            // Tambahkan entri GudangCek hanya jika tidak ada barang sebelumnya
            $gudangCek = new GudangCek();
            $gudangCek->sales_id = $request->sales_id;
            $gudangCek->status = 'Waiting_Cek';
            $gudangCek->save();

            // Buat status 'waiting_psb' di tabel jadwal_psb
            $this->createWaitingPsb($request->sales_id);

            // Commit transaksi
            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Berhasil memasukkan data',
            ], 200);
        } catch (\Exception $e) {
            // Tangkap kesalahan dan rollback transaksi jika terjadi kesalahan
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan: ' . $e->getMessage(),
            ], 500);
        }
    }

    public function createWaitingPsb($sales_id)
    {
        JadwalPsb::create([
            'sales_id' => $sales_id,
            'status' => 'Waiting_Psb',
        ]);
    }
}
