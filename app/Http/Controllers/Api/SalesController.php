<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sales;
// use Dotenv\Validator;
use Illuminate\Database\Eloquent\Casts\Json;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class SalesController extends Controller
{

    public function index()
    {
        $data = Sales::orderBy('plgName', 'asc')->get();
        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }


    public function showById($id)
    {
        $data = Sales::find($id);

        if ($data) {
            return response()->json([
                'status' => true,
                'message' => 'Data ditemukan',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }
    public function showByStatus($status)
    {
        $data = Sales::where('status', $status)->get();

        if ($data->isNotEmpty()) {
            return response()->json([
                'status' => true,
                'message' => 'Data ditemukan',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
    }


    public function generateTicketNumber()
    {
        $lastTicket = Sales::max('id');
        $tahun = date('y');
        $bulan = date('m');
        $nomerTicket = $lastTicket ? intval($lastTicket) + 1 : 1;
        $noTicket = '#' . $tahun . $bulan .  $nomerTicket;
        return $noTicket;
    }


    public function store(Request $request)
    {
        $dataSales = new Sales;

        $rules = [
            'plgName' => 'required',
            'status' => 'required',
            'paket' => 'required',
            'harga' => 'required',
            'alamat' => [
                'required',
                'regex:/^jalan [\w\s]+ rt \d+ rw \d+ kelurahan\/desa [\w\s]+ kecamatan [\w\s]+ kabupaten [\w\s]+$/i',
            ],
            'noHp' => 'required|regex:/^\+62[0-9]{9,15}$/',
            'email' => 'required|email',
            'lat' => 'required',
            'lon' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        // Atur pesan khusus untuk aturan nomor HP
        $validator->setAttributeNames([
            'noHp' => 'Nomor HP'
        ]);

        // Atur pesan khusus untuk aturan nomor HP yang sesuai
        $validator->setCustomMessages([
            'noHp.regex' => 'Format :attribute harus dimulai dengan +62 dan memiliki panjang antara 9 hingga 15 digit.',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memasukkan data',
                'errors' => $validator->errors()
            ], 422);
        }

        // Memisahkan alamat menjadi komponen yang sesuai
        $components = explode(' ', $request->alamat);

        // Simpan data ke database dalam transaksi
        try {
            DB::beginTransaction();

            // Setel nilai input ke model
            $dataSales->fill([
                'plgName' => $request->plgName,
                'status' => $request->status,
                'paket' => $request->paket,
                'harga' => $request->harga,
                'jalan' => $components[1],
                'rt' => $components[3],
                'rw' => $components[5],
                'kelurahan_desa' => $components[7],
                'kecamatan' => $components[9],
                'kabupaten' => $components[11],
                'noHp' => $request->noHp,
                'email' => $request->email,
                'lat' => $request->lat,
                'lon' => $request->lon,
                'noTicket' => $this->generateTicketNumber(),
            ])->save();

            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Data berhasil disimpan.',
                'noTicket' => $dataSales->noTicket,
            ], 201);
        } catch (\Exception $e) {
            // Tangkap kesalahan dan batalkan transaksi
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message' => 'Terjadi kesalahan: ' . $e->getMessage(),
            ], 500);
        }
    }
}
