<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AuthController extends Controller
{

    // public function login(Request $request)
    // {
    //     $validate = Validator::make($request->all(), [
    //         'email' => 'required',
    //         'password' => 'required',
    //     ]);

    //     if ($validate->fails()) {
    //         $respon = [
    //             'message' => 'Validator error',
    //             'errors' => $validate->errors(),
    //         ];

    //         return response()->json($respon, 401);
    //     } else {
    //         $credentials = [
    //             'email' => $request->input('email'),
    //             'password' => $request->input('password'),
    //             // 'status' => true,
    //         ];

    //         if (!auth()->attempt($credentials)) {
    //             $respon = [
    //                 'message' => 'Login gagal',
    //                 'errors' => null,
    //             ];
    //             return response()->json($respon, 401);
    //         }

    //         $user = User::where('email', $request->input('email'))->first();

    //         $respon = [
    //             'message' => 'Login berhasil',
    //             'access_token' => $user->createToken('authToken')->plainTextToken,
    //             'token_type' => 'Bearer',
    //             'data' => [
    //                 'user' => $user,
    //             ],
    //         ];
    //         return response()->json($respon, 200);
    //     }
    // }

    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validate->fails()) {
            $respon = [
                'message' => 'Validator error',
                'errors' => $validate->errors(),
            ];

            return response()->json($respon, 401);
        } else {
            $credentials = [
                'email' => $request->input('email'),
                'password' => $request->input('password'),
            ];

            if (!auth()->attempt($credentials)) {
                $respon = [
                    'message' => 'Login gagal',
                    'errors' => null,
                ];
                return response()->json($respon, 401);
            }

            $user = User::where('email', $request->input('email'))->first();

            if ($user->hasRole('sales') || $user->hasRole('teknisi')) {
                $respon = [
                    'message' => 'Login berhasil',
                    'access_token' => $user->createToken('authToken')->plainTextToken,
                    'token_type' => 'Bearer',
                    'data' => [
                        'user' => $user,
                    ],
                ];
                return response()->json($respon, 200);
            } else {
                // Jika pengguna tidak memiliki peran yang diizinkan
                auth()->logout(); // Logout pengguna
                $respon = [
                    'message' => 'Anda tidak memiliki izin untuk melakukan login',
                    'errors' => null,
                ];
                return response()->json($respon, 403);
            }
        }
    }




    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();

        $respon = [
            'message' => 'Logout berhasil',
        ];

        return response()->json($respon, 200);
    }
    // public function logout(Request $request)
    // {
    //     $user = auth()->user();

    //     if ($user) {
    //         $user->update(['status' => false]);
    //         auth()->logout();
    //     }

    //     $respon = [
    //         'message' => 'Logout berhasil',
    //     ];

    //     return response()->json($respon, 200);
    // }



    //
    //     public function Register(Request $request){

    //     //     $validator = Validator::make($request->all(),[
    //     //         'name'=>'required',
    //     //         'email' => 'required|email',
    //     //         'password' => 'required',
    //     //         'confirm_password' => 'required|same:password',

    //     //     ]);

    //     //     if($validator->fails()){
    //     //         return response()->json([
    //     //             'succes'=> false,
    //     //             'message'=>'something wrong',
    //     //             'data'=>$validator->errors()
    //     //         ]);
    //     //     }

    //     //     $input = $request->all();
    //     //     $input['password']=bcrypt($input['password']);
    //     //     $user = User::create($input);


    //     //     $success['email'] = $user->email;
    //     //     $success['name'] = $user->name;

    //     //     return response()->json([
    //     //         'success'=>true,
    //     //         'message'=>'register successful',
    //     //         'data'=> $success
    //     //     ]);

    //     //
    //  }
    // public function Login(Request $request)
    // {

    //     $credentials = [
    //         'email' => $request->input('email'),
    //         'password' => $request->input('password'),
    //         'status' => true,
    //     ];
    //     if(Auth::attempt(['email'=> $request->email,'password'=> $request->password])){

    //     $user = User::where('email', $request->email)->first();
    //     $success['token']= $user->createToken('auth_token')->plainTextToken;
    //     $success['name']= $user->name;
    //     return response()->json([
    //         'succes'=> true,
    //         'message'=>'Login Berhasil',
    //         'data'=> $success,

    //     ]);

    // }else {
    //     return response()->json([
    //         'succes'=> false,
    //          'message'=>'Cek kembali email dan password',
    //          'data'=> null
    //     ]);
    // }

    // }
    // public function login(Request $request)
    // {
    //     $validate = Validator::make($request->all(), [
    //         'email' => 'required',
    //         'password' => 'required',
    //     ]);

    //     if ($validate->fails()) {
    //         $respon = [
    //             'message' => 'Validator error',
    //             'errors' => $validate->errors(),
    //         ];
    //         return response()->json($respon, 200);
    //     } else {
    //         $credentials = [
    //             'email' => $request->input('email'),
    //             'password' => $request->input('password'),
    //             'status' => true,
    //         ];

    //         if (!auth()->attempt($credentials)) {
    //             $respon = [
    //                 'message' => 'Login gagal',
    //                 'errors' => null,
    //             ];
    //             return response()->json($respon, 401);
    //         }

    //         $user = User::where('email', $request->input('email'))->first();

    //         $respon = [
    //             'message' => 'Login berhasil',
    //             'access_token' => $user->createToken('authToken')->plainTextToken,
    //             'token_type' => 'Bearer',
    //             'data' => [
    //                 'user' => $user,
    //             ],
    //         ];
    //         return response()->json($respon, 200);
    //     }
    // }

    // public function login(Request $request)
    // {
    //     $login = Auth::Attempt($request->all());
    //     if ($login) {
    //         $user = Auth::user();
    //         $user->api_token = Str::random(100);
    //         $user->save();
    //         // $user->makeVisible('api_token');

    //         return response()->json([
    //             'response_code' => 200,
    //             'message' => 'Login Berhasil',
    //             'content' => $user
    //         ]);
    //     }else{
    //         return response()->json([
    //             'response_code' => 404,
    //             'message' => 'Username atau Password Tidak Ditemukan!'
    //         ]);
    //     }
    // }


}
