<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\JadwalPsb;
use App\Models\Paying;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class PayingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Paying::orderBy('metode', 'asc')->get();
        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // Validasi data pembayaran
        $rules = [
            'metode' => 'required|in:Tunai,Transfer',
            'bukti_pembayaran' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20480',
            'sales_id' => 'required|exists:sales,id',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memasukkan data',
                'data' => $validator->errors()
            ], 404);
        }

        // Check if sales_id already exists in Paying table
        $existingPaying = Paying::where('sales_id', $request->sales_id)->first();
        if ($existingPaying) {
            return response()->json([
                'status' => false,
                'message' => 'Data for sales_id already exists. Cannot process.'
            ], 400);
        }

        // Simpan data pembayaran dan nama file gambar bukti pembayaran dalam transaksi
        DB::transaction(function () use ($request) {

            // Simpan gambar bukti pembayaran di folder storage
            $image = $request->file('bukti_pembayaran');
            // $filename = $image->getClientOriginalName();
            $image_uploaded = $image->store('public/posts');

            $imagePath = URL::to('/') . Storage::url($image_uploaded);

            $dataPaying = new Paying();
            $dataPaying->metode = $request->metode;

            // Simpan nama file ke dalam basis data
            $dataPaying->bukti_pembayaran = $imagePath;

            $dataPaying->sales_id = $request->sales_id;
            $dataPaying->save();


            // Update status in jadwal_psb table
            JadwalPsb::updateOrCreate(
                ['sales_id' => $request->sales_id],
                [
                    'status' => 'Aktivasi_Psb',
                    'status_teknisi' => 'Waiting_Aktivasi'
                ]
            );

        });

        return response()->json([
            'status' => true,
            'message' => 'Berhasil memasukkan data'
        ]);
    }

}

