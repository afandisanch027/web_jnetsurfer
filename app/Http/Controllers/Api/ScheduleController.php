<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Sales;
use App\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Schedule::orderBy('team', 'asc')->get();
        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }


    public function showByStatus($status_teknisi)
    {
        $data = Schedule::where('status_teknisi', $status_teknisi)
            ->orderBy('id', 'desc') // Mengurutkan berdasarkan created_at secara descending (terbaru dulu)
            ->get();

        if ($data->isEmpty()) {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $dataSchedule = new Schedule();

        $rules = [
            'tgl_psb' => 'required',
            'status' => 'required',
            'team' => 'required'


        ];
        $validator = Validator($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal memasukkan data',
                'data' => $validator->errors()
            ], 404);
        }
        $dataSchedule->tgl_psb = $request->tgl_psb;
        $dataSchedule->status = $request->status;
        $dataSchedule->team = $request->team;


        $post = $dataSchedule->save();

        return response()->json([
            'status' => true,
            'message' => 'Berhasil memasukkan data'
        ]);
    }


    public function update(Request $request, string $id)
    {
        $dataSchedule = Schedule::find($id);

        if (empty($dataSchedule)) {
            return response()->json([
                'status_teknisi' => false,
                'message' => 'Data tidak ditemukan'
            ], 400);
        }

        $rules = [
            'status_teknisi' => 'required|in:Proses_Teknisi_Psb'
        ];

        $validator = Validator($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status_teknisi' => false,
                'message' => 'Gagal melakukan update data',
                'data' => $validator->errors()
            ], 404);
        }

        // Update status_teknisi jika validasi berhasil
        $dataSchedule->status_teknisi = $request->input('status_teknisi');
        $dataSchedule->save();

        return response()->json([
            'status_teknisi' => true,
            'message' => 'Berhasil melakukan update data'
        ], 200);
    }


}
