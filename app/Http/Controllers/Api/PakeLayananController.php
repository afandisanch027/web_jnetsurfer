<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PaketLayanan;
use Illuminate\Http\Request;

class PakeLayananController extends Controller
{
    public function index()
    {
        $data = PaketLayanan::orderBy('id','asc')->get();
        return response()->json([
            'status'=> true,
            'message'=> 'Data ditemukan',
            'data'=> $data
        ],200 );
    }
}
