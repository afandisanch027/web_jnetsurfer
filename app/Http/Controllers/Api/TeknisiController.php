<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\Teknisi;
use App\Models\Sales;
use Dotenv\Validator;
use Illuminate\Database\Eloquent\Casts\Json;
use Illuminate\Http\Request;

class TeknisiController extends Controller
{

    public function index()
    {
        $data = Teknisi::orderBy('sales_id','asc')->get();
        return response()->json([
            'status'=> true,
            'message'=> 'Data ditemukan',
            'data'=> $data
        ],200 );
    } 


    public function update(Request $request, string $id)
    {
        $dataTeknisi = Teknisi::find($id);
        if (empty($dataTeknisi)) {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }
        $rules = [
            'status' => 'required|in:Proses_Survei',

        ];
        $validator = Validator($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'message' => 'Gagal melakukan update data, karenea status harus "Proses_Survei"',
                'data' => $validator->errors()
            ], 404);
        }
        $dataTeknisi->status = $request->status;

        $post = $dataTeknisi->save();
        return response()->json([
            'status' => true,
            'message' => 'Berhasil melakukan update data'
        ]);
    }

    public function showByStatus($status)
    {
        $data = Teknisi::where('status', $status)->get();

        if ($data->isEmpty()) {
            return response()->json([
                'status' => false,
                'message' => 'Data tidak ditemukan'
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'Data ditemukan',
            'data' => $data
        ], 200);
    }

    public function getSelectedData()
    {
        $data1 = Sales::select('plgName', 'paket', 'harga', 'alamat', 'lat', 'lon', 'noHp')->get();
        $data2 = Teknisi::select('tgl_survei', 'status')->get();
        $data3 = Barang::select('nama','qty')->get();

        $mergedData = [
            'data1' => $data1,
            'data2' => $data2,
            'data3' => $data3,
        ];

        return response()->json($mergedData);
    }
}
