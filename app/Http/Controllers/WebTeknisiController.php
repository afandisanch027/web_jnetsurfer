<?php

namespace App\Http\Controllers;

use App\Models\JadwalPsb;
use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Http\Request;

class WebTeknisiController extends Controller
{
    // public function index()
    // {
    //     $salesData = Sales::all();
    //     $teknisiData = Teknisi::all();

    //     return view('teknisi.hasil_survei', compact('salesData', 'teknisiData'));
    // }
    public function index()
    {
        $salesData = Sales::paginate(10); // Mengambil 10 item per halaman
        $teknisiData = Teknisi::all();

        return view('teknisi.hasil_survei', compact('salesData', 'teknisiData'));
    }



    public function psb()
    {
        $salesData = Sales::paginate(10);
        $jadwal_psbData = JadwalPsb::all();
        
        return view('teknisi.hasil_psb', compact('salesData', 'jadwal_psbData'));
    }
}
