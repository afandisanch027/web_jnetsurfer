<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\KeluarBarang;
use App\Models\MasterBarang;
use App\Models\MasukBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterBarangController extends Controller
{
    public function index()
    {
        $masterBarangs = MasterBarang::paginate(10);
        return view('inventory.barang.data_barang', compact('masterBarangs'));
    }

    public function createFormDataBarang()
    {
        return view('inventory.barang.form_data_barang');
    }

    public function submitDataBarang(Request $request)
    {
        $validatedData = $request->validate([
            'nama_barang' => 'required',
            'kategori' => 'required',
            'satuan' => 'required',
            'type' => 'required',
            'safety_stock' => 'required',
        ]);
        MasterBarang::create($validatedData);
        return redirect()->route('inventory.data_barang')->with('success', 'Data berhasil dibuat!');
    }

    public function editDataBarang($id)
    {
        $databarang = MasterBarang::find($id);
        if (!$databarang) {
            return redirect()->route('data_barang')->with('error', 'Data tidak ditemukan');
        }
        return view('inventory.barang.edit_data_barang', compact('databarang'));
    }

    public function submitEditDataBarang(Request $request)
    {
        $request->validate([
            'nama_barang' => 'required',
            'kategori' => 'required',
            'satuan' => 'required',
            'type' => 'required',
            'safety_stock' => 'required',
        ]);
        $data = [
            'nama_barang' => $request->input('nama_barang'),
            'kategori' => $request->input('kategori'),
            'satuan' => $request->input('satuan'),
            'type' => $request->input('type'),
            'safety_stock' => $request->input('safety_stock'),
        ];
        MasterBarang::updateOrInsert(
            ['id' => $request->input('id')],
            $data
        );
        return redirect()->route('inventory.data_barang')->with('success', 'Data berhasil diperbarui');
    }

    //Barang Masuk
    public function barang_masuk()
    {
        $barangmasuk = MasukBarang::paginate(10);
        return view('inventory.barang.barang_masuk', compact('barangmasuk'));
    }

    public function createFormBarangMasuk()
    {
        $masterBarangs = MasterBarang::all();

        return view('inventory.barang.form_barang_masuk', ['masterBarangs' => $masterBarangs]);
    }

    public function submitBarangMasuk(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'nama_barang' => 'required',
            'quantity' => 'required|numeric',
        ]);

        try {
            $barangMasuk = new MasukBarang(); // Ganti MasukBarang menjadi KeluarBarang
            $barangMasuk->id_barang = $request->input('nama_barang');
            $barangMasuk->tanggal = $request->input('tanggal');
            $barangMasuk->quantity = $request->input('quantity');
            $barangMasuk->save();

            return redirect()->route('inventory.barang_masuk')->with('success', 'Data berhasil disimpan!');
        } catch (\Exception $e) {
            // Perhatikan bahwa variabel $id tidak didefinisikan sebelumnya, jika Anda ingin menggunakan ID di sini, pastikan untuk mendapatkan ID dari objek yang sesuai
            return redirect()->route('inventory.barang.form_barang_masuk')->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }

    public function editBarangMasuk($id)
    {
        $barangmasuk = MasukBarang::find($id);
        $masterBarangs = MasterBarang::all();
        if (!$barangmasuk) {
            return redirect()->route('barang_masuk')->with('error', 'Data tidak ditemukan');
        }
        return view('inventory.barang.edit_barang_masuk', compact('barangmasuk', 'masterBarangs'));
    }

    public function submitEditBarangMasuk(Request $request, $id)
    {
        $request->validate([
            'id_barang' => 'required',
            'tanggal' => 'required',
            'quantity' => 'required|numeric',
        ]);
        try {
            $barangMasuk = MasukBarang::find($id);
            $barangMasuk->id_barang = $request->input('id_barang');
            $barangMasuk->tanggal = $request->input('tanggal');
            $barangMasuk->quantity = $request->input('quantity');
            $barangMasuk->save();
            return redirect()->route('inventory.barang_masuk')->with('success', 'Data berhasil disimpan!');
        } catch (\Exception $e) {
            return redirect()->route('inventory.barang.edit_barang_masuk', ['id' => $id])->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }


    // Barang_Keluar
    public function barang_keluar()
    {
        $barangkeluar = KeluarBarang::paginate(10);
        return view('inventory.barang.barang_keluar', compact('barangkeluar'));
    }


    public function createFormBarangKeluar()
    {
        $masterBarangs = MasterBarang::all();

        return view('inventory.barang.form_barang_keluar', ['masterBarangs' => $masterBarangs]);
    }

    public function submitBarangKeluar(Request $request)
    {
        $request->validate([
            'tanggal' => 'required',
            'nama_barang' => 'required',
            'quantity' => 'required|numeric',
        ]);

        try {
            $barangKeluar = new KeluarBarang(); // Ganti MasukBarang menjadi KeluarBarang
            $barangKeluar->id_barang = $request->input('nama_barang');
            $barangKeluar->tanggal = $request->input('tanggal');
            $barangKeluar->quantity = $request->input('quantity');
            $barangKeluar->save();

            return redirect()->route('inventory.barang_keluar')->with('success', 'Data berhasil disimpan!');
        } catch (\Exception $e) {
            // Perhatikan bahwa variabel $id tidak didefinisikan sebelumnya, jika Anda ingin menggunakan ID di sini, pastikan untuk mendapatkan ID dari objek yang sesuai
            return redirect()->route('inventory.barang.form_barang_keluar')->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }

    public function editBarangKeluar($id)
    {
        $barangkeluar = KeluarBarang::find($id);
        $masterBarangs = MasterBarang::all();
        if (!$barangkeluar) {
            return redirect()->route('barang_keluar')->with('error', 'Data tidak ditemukan');
        }
        return view('inventory.barang.edit_barang_keluar', compact('barangkeluar', 'masterBarangs'));
    }

    public function submitEditBarangKeluar(Request $request, $id)
    {
        $request->validate([
            'id_barang' => 'required',
            'tanggal' => 'required',
            'quantity' => 'required|numeric',
        ]);
        try {
            $barangkeluar = KeluarBarang::find($id);
            $barangkeluar->id_barang = $request->input('id_barang');
            $barangkeluar->tanggal = $request->input('tanggal');
            $barangkeluar->quantity = $request->input('quantity');
            $barangkeluar->save();
            return redirect()->route('inventory.barang_keluar')->with('success', 'Data berhasil disimpan!');
        } catch (\Exception $e) {
            return redirect()->route('inventory.barang.edit_barang_keluar', ['id' => $id])->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }


    //stock_barang
    // public function stockBarang()
    // {
    //     $masterBarangs = MasterBarang::all();

    //     // Inisialisasi $barangMasuk dan $barangKeluar sebagai array kosong
    //     $barangMasuk = [];
    //     $barangKeluar = [];

    //     // Ambil jumlah masuk untuk setiap barang
    //     $masukBarangs = MasukBarang::select(DB::raw('id_barang, SUM(quantity) as total_masuk'))
    //         ->groupBy('id_barang')
    //         ->pluck('total_masuk', 'id_barang')
    //         ->all();

    //     // Ambil jumlah keluar untuk setiap barang
    //     $keluarBarangs = KeluarBarang::select(DB::raw('id_barang, SUM(quantity) as total_keluar'))
    //         ->groupBy('id_barang')
    //         ->pluck('total_keluar', 'id_barang')
    //         ->all();

    //     // Mengisi $barangMasuk dan $barangKeluar dengan nilai yang diambil dari database
    //     foreach ($masterBarangs as $barang) {
    //         $idBarang = $barang->id;
    //         $barangMasuk[$idBarang] = $masukBarangs[$idBarang] ?? 0;
    //         $barangKeluar[$idBarang] = $keluarBarangs[$idBarang] ?? 0;
    //     }

    //     // Format data untuk ditampilkan di tampilan Blade
    //     $stokBarang = [];
    //     foreach ($masterBarangs as $barang) {
    //         $idBarang = $barang->id;
    //         $masuk = $barangMasuk[$idBarang];
    //         $keluar = $barangKeluar[$idBarang];
    //         $stokBarang[$idBarang] = [
    //             'masuk' => $masuk,
    //             'keluar' => $keluar,
    //             'sisa' => $masuk - $keluar,
    //         ];
    //     }

    //     return view('inventory.barang.stock_barang', compact('masterBarangs', 'stokBarang', 'barangMasuk', 'barangKeluar'));
    // }

    public function stockBarang()
    {
        $masterBarangs = MasterBarang::all();

        // Inisialisasi $barangMasuk, $barangKeluar, dan $qtyOuts sebagai array kosong
        $barangMasuk = [];
        $barangKeluar = [];

        // Ambil jumlah masuk untuk setiap barang
        $masukBarangs = MasukBarang::select(DB::raw('id_barang, SUM(quantity) as total_masuk'))
            ->groupBy('id_barang')
            ->pluck('total_masuk', 'id_barang')
            ->all();

        // Ambil jumlah keluar untuk setiap barang dari tabel keluar_barang
        $keluarBarangs = KeluarBarang::select(DB::raw('id_barang, SUM(quantity) as total_keluar'))
            ->groupBy('id_barang')
            ->pluck('total_keluar', 'id_barang')
            ->all();

        // Ambil qty_out untuk setiap barang dari tabel barangs
        $qtyOuts = Barang::select(DB::raw('barang_id, SUM(qty_out) as qty_out'))
            ->groupBy('barang_id')
            ->pluck('qty_out', 'barang_id')
            ->all();

        // Mengisi $barangMasuk dan $barangKeluar dengan nilai yang diambil dari database
        foreach ($masterBarangs as $barang) {
            $idBarang = $barang->id;
            $barangMasuk[$idBarang] = $masukBarangs[$idBarang] ?? 0;

            // Periksa apakah $idBarang ada dalam array $keluarBarangs
            if (isset($keluarBarangs[$idBarang])) {
                // Jika ada, tambahkan qty_out di tabel barangs dengan jumlah keluar dari tabel keluar_barang
                $barangKeluar[$idBarang] = $keluarBarangs[$idBarang] + ($qtyOuts[$idBarang] ?? 0);
            } else {
                // Jika tidak, set nilai ke qty_outs atau 0 jika tidak ada
                $barangKeluar[$idBarang] = $qtyOuts[$idBarang] ?? 0;
            }
        }

        // Format data untuk ditampilkan di tampilan Blade
        $stokBarang = [];
        foreach ($masterBarangs as $barang) {
            $idBarang = $barang->id;
            $masuk = $barangMasuk[$idBarang];
            $keluar = $barangKeluar[$idBarang];
            $sisa = $masuk - $keluar;
            $stokBarang[$idBarang] = [
                'masuk' => $masuk,
                'keluar' => $keluar,
                'sisa' => $sisa,
            ];
        }

        return view('inventory.barang.stock_barang', compact('masterBarangs', 'stokBarang', 'barangMasuk', 'barangKeluar'));
    }
}
