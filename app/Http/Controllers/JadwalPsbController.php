<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\GudangCek;
use App\Models\JadwalPsb;
use App\Models\Paying;
use App\Models\Sales;
use App\Models\Teknisi;
use Illuminate\Http\Request;

class JadwalPsbController extends Controller
{
    public function showJadwalPsbForm($sales_id)
    {
        // Dapatkan data penjualan berdasarkan ID
        $salesData = Sales::find($sales_id);
        $barangs = $salesData->barangs;
        $jadwalPsb = $salesData->jadwalPsb;

        // Periksa jika data penjualan ditemukan
        if (!$salesData || !$jadwalPsb) {
            abort(404);
        }

        return view('teknisi.form_jadwal_psb', compact('salesData', 'barangs', 'jadwalPsb'));
    }

    public function submitJadwalPsb(Request $request)
    {
        // Validasi data dari formulir
        $request->validate([
            'tgl_psb' => 'required|date',
            'team' => 'required',
        ]);

        $sales_id = $request->input('sales_id');

        // Periksa apakah sudah ada jadwal PSB untuk sales_id ini
        $existingJadwalPsb = JadwalPsb::where('sales_id', $sales_id)->first();

        if (!$existingJadwalPsb) {
            return redirect()->back()->with('error', 'Tidak ada jadwal PSB untuk sales_id ini.');
        }

        // Periksa status di tabel GudangCek
        $statusGudangCek = GudangCek::where('sales_id', $sales_id)->value('status');

        // Tentukan status baru berdasarkan status di tabel GudangCek
        $newStatusJadwalPsb = ($statusGudangCek == 'Waiting_Cek') ? 'Proses_Psb' : 'Selesai_Psb';

        // Update data di database tabel JadwalPsb
        $existingJadwalPsb->update([
            'tgl_psb' => $request->input('tgl_psb'),
            'team' => $request->input('team'),
            'status' => $newStatusJadwalPsb, // Update status di tabel JadwalPsb
        ]);

        // Jika status GudangCek adalah 'Proses_Cek', ubah menjadi 'Selesai_Cek'
        if ($statusGudangCek == 'Proses_Cek') {
            GudangCek::where('sales_id', $sales_id)->update(['status' => 'Selesai_Cek']);

            // Ubah status_teknisi menjadi 'Waiting_Teknisi_Psb'
            JadwalPsb::updateOrCreate(
                ['sales_id' => $sales_id],
                ['status_teknisi' => 'Waiting_Teknisi_Psb']
            );
        }

        return redirect()->route('teknisi.psb')->with('success', 'Jadwal PSB berhasil diperbarui.');
    }






    public function showAktivasiPsbForm($sales_id)
    {
        // Dapatkan data penjualan berdasarkan ID
        $salesData = Sales::find($sales_id);

        // Dapatkan data pembayaran berdasarkan ID penjualan
        $paying = Paying::where('sales_id', $sales_id)->get();

        // Periksa jika data penjualan ditemukan
        if (!$salesData) {
            abort(404);
        }

        return view('teknisi.form_aktivasi_psb', compact('salesData', 'paying'));
    }


    public function aktivasiPsb(Request $request)
    {
        // Validasi request
        $request->validate([
            'sales_id' => 'required|exists:sales,id',
        ]);

        // Ubah status di tabel jadwal_psb
        JadwalPsb::updateOrCreate(
            ['sales_id' => $request->sales_id],
            [
                'status_teknisi' => 'Selesai_Teknisi_Psb',
                'status' => 'Selesai', // Tambahkan perubahan status menjadi 'Selesai'
                'tgl_selesai_psb' => now(), // Menggunakan fungsi now() untuk mendapatkan waktu saat ini
            ]
        );

        return redirect()->route('teknisi.psb')->with('success', 'Status berhasil diubah menjadi Selesai_Teknisi_Psb');
    }
}
