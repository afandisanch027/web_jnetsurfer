<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Models\PaketLayanan;
use Illuminate\Http\Request;


class PaketLayananController extends Controller
{
    public function index()
    {
        $pagination = 10;
        $paketLayanan = PaketLayanan::paginate($pagination);
        return view('admin.paket_layanan.paket_layanan', ['paketlayanan' => $paketLayanan]);
    }

    public function createFormPaketLayanan()
    {
        return view('admin.paket_layanan.form_paket_layanan');
    }

    public function submitPaketLayanan(Request $request)
    {
        $validatedData = $request->validate([
            'kecepatan' => 'required',
            'satuan' => 'required',
            'harga' => 'required',
            'type' => 'required',
        ]);
        PaketLayanan::create($validatedData);
        return redirect()->route('paketlayanan.index')->with('success', 'Data berhasil dibuat!');
    }


 }
